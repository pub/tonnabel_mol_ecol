#!/bin/bash
set -x

if [ $# -eq 0 ]
then
  ARCH="i386 amd64"
else
  ARCH=$@
fi

FLAGS="-m32 -m64"
  
for arch in $ARCH
do
  echo "generation $arch ..."
  make clean

  if [ "$arch" == "i386" ]
  then
    rm CMakeCache.txt
    cmake . -DCMAKE_C_FLAGS="-m32" -DCMAKE_CXX_FLAGS="-m32" -DQt5Widgets_DIR=/opt/Qti386/5.3/gcc/lib/cmake/Qt5Widgets -DARCH="${arch}" && make VERBOSE=1
    cpack
    cpack -G DEB 
    continue
  fi

  if [ "$arch" == "amd64" ]
  then
    rm CMakeCache.txt
    cmake . -DQt5Widgets_DIR=/home/jfrey/Tools/QT/x86_64/lib/cmake/Qt5Widgets -DBUILD_DIST=ON -DBUILD_GUI=ON -DBUILD_STATIC=ON -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_FLAGS="-m64" -DCMAKE_CXX_FLAGS="-m64" && make VERBOSE=1 
    cpack
    cpack -G DEB
    continue
  fi

  if [ "$arch" == "MACOSX" ]
  then
    rm CMakeListCache.txt
    export MACOSX_DEPLOYMENT_TARGET=10.7
    cmake . -DBOOST_ROOT=/users/jef/Tools/boostinstall -DQt5Widgets_DIR=/Users/jef/Tools/Qt/5.3/clang_64/lib/cmake/Qt5Widgets -DBUILD_DIST=ON -DBUILD_GUI=ON -DCMAKE_OSX_SYSROOT=/Volumes/Xcode/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.7.sdk -DCMAKE_OSX_DEPLOYMENT_TARGET=10.7
    make
    cpack -G Bundle --verbose
    continue
  fi

done
