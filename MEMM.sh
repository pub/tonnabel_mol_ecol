#!/bin/bash

MEMM_BUNDLE="`echo $0 | sed -e 's/\/Contents\/MacOS\/MEMM//'`"
MEMM_RESOURCES="$MEMM_BUNDLE/Contents/Resources"
MEMM_FRAMEWORKS="$MEMM_BUNDLE/Contents/Frameworks"

export DYLD_LIBRARY_PATH=“$MEMM_RESOURCES/lib:$MEMM_FRAMEWORKS/:$DYLB_LIBRARY_PATH"
#export DYLD_PRINT_LIBRARIES=1
export PATH=“$MEMM_RESOURCES/bin:$PATH"

#exec $MEMM_RESOURCES/bin/MEMM_GUI

# add .memm config file in home directory
if [ -e ~/.memm ]
then
  rm ~/.memm
fi
touch ~/.memm
echo -e export DYLD_LIBRARY_PATH=$MEMM_RESOURCES/lib:$MEMM_FRAMEWORKS/:$DYLB_LIBRARY_PATH \\n export PATH=$MEMM_RESOURCES/bin:$PATH >> ~/.memm
echo echo -e \\\\\n############\\\\\n### MEMM\\\ ###\\\\\n############ >> ~/.memm
echo echo -e \\\\n to\\\ run\\\ MEMM\\\ :\\\ \\\\n\\\\t\\\$\\\>\\\ MEMM\\\ -p\\\ parameters.xml \\\\\\n >> ~/.memm

# add in .bash_profile to source .memm
DIRNAME=`dirname ~/. `
if [ ! -e ~/.bash_profile ]
then
  touch ~/.bash_profile
  echo -e source $DIRNAME/.memm >> ~/.bash_profile
else
  #sed on macosx does not working
  #sed ‘/\.memm/d’ $DIRNAME/.bash_profile
  RES=`grep memm ~/.bash_profile`
  if [ -z RES ]
  then
    echo -e source $DIRNAME/.memm >> ~/.bash_profile
  fi
fi

# open terminal
open -a Terminal ~/