:: cmake . -G "MinGW Makefiles" -DBUILD_GUI=OFF -DLIBXML2_LIBRARIES="C:\libxml2\libxml2-2.9.2-win32-x86\lib\libxml2.dll.a" -DLIBXML2_INCLUDE_DIR="C:\libxml2\libxml2-2.9.2-win32-x86\include\libxml2\" -DBOOST_ROOT="C:\boost\boost_1_56_0" -DLIBXML2_DEFINITIONS="-lxml2 -lz -liconv -l ws2_32 -I C:\libxml2\libxml2-2.9.2-win32-x86\include\libxml2\ -L C:\libxml2\libxml2-2.9.2-win32-x86\lib\"
:: cmake . -G "MinGW Makefiles" -DBUILD_GUI=ON -DLIBXML2_LIBRARIES="C:\libxml2\libxml2-2.9.2-win32-x86\lib\libxml2.dll.a" -DLIBXML2_INCLUDE_DIR="C:\libxml2\libxml2-2.9.2-win32-x86\include\libxml2" -DBOOST_ROOT="C:\boost\boost_1_56_0" -DLIBXML2_DEFINITIONS="" -DQt5Widgets_DIR=C:\Qt\5.3\mingw482_32\lib\cmake\Qt5Widgets
:: cmake . -G "MinGW Makefiles" -DBUILD_GUI=OFF -DLIBXML2_LIBRARIES="C:\libxml2\libxml2-2.9.1-win32-x86\lib\libxml2.dll.a" -DLIBXML2_INCLUDE_DIR=C:\libxml2\libxml2-2.9.1-win32-x86\include\libxml2\ -DBOOST_ROOT="C:\boost\boost_1_56_0" -DLIBXML2_DEFINITIONS="" -DARCH=i386 -DCMAKE_C_FLAGS="-m32" -DCMAKE_CXX_FLAGS="-m32" 

:: Compilation 32 bits
set PATH=C:\Program Files (x86)\CMake\bin;C:\libxml2\libxml2-2.9.2-win32-x86\bin;C:\MinGW\i686-4.9.1-release-posix-dwarf-rt_v3-rev2\mingw32\bin\;C:\Qt\Static\5.3.1\bin\
mingw32-make dist clean
:: del CMakeCache.txt
:: rmdir CMakeFiles /s
:: cmake . -G "MinGW Makefiles" -DBUILD_GUI=ON -DLIBXML2_LIBRARIES="C:\libxml2\libxml2-2.9.2-win32-x86\lib\libxml2.a" -DLIBXML2_INCLUDE_DIR="C:\libxml2\libxml2-2.9.2-win32-x86\include\libxml2" -DBOOST_ROOT="C:\boost\boost_1_56_0_bin_64" -DLIBXML2_DEFINITIONS="" -DARCH=i686 -DCMAKE_C_FLAGS="-m32" -DCMAKE_CXX_FLAGS="-m32" -DCMAKE_BUILD_TYPE=Debug -DQt5Widgets_DIR=C:\Qt\5.3\mingw482_32\lib\cmake\Qt5Widgets
:: cmake . -G "MinGW Makefiles" -DBUILD_STATIC=ON -DBUILD_GUI=ON -DLIBXML2_LIBRARIES="C:\libxml2\libxml2-2.9.2-win32-x86\lib\libxml2.dll.a" -DLIBXML2_INCLUDE_DIR="C:\libxml2\libxml2-2.9.2-win32-x86\include\libxml2" -DBOOST_ROOT="C:\boost\boost_1_56_0" -DLIBXML2_DEFINITIONS="" -DCMAKE_BUILD_TYPE=Debug -DQt5Widgets_DIR=C:\Qt\5.3\mingw482_32\lib\cmake\Qt5Widgets -DBUILD_DIST=ON -DCMAKE_INCLUDE_PATH="C:\Program Files\mingw-w64\i686-4.9.1-release-posix-dwarf-rt_v3-rev2\mingw32\include" -DCMAKE_LIBRARY_PATH="C:\Program Files\mingw-w64\i686-4.9.1-release-posix-dwarf-rt_v3-rev2\mingw32\lib" -DARCH=i686 -DCMAKE_C_FLAGS="-m32" -DCMAKE_CXX_FLAGS="-m32" 
cmake . -G "MinGW Makefiles" -DBUILD_STATIC=ON -DBUILD_GUI=ON -DBOOST_ROOT="C:/boost/boost_1_56_0" -DCMAKE_BUILD_TYPE=Release -DLIBXML2_LIBRARIES="C:\libxml2\libxml2-2.9.2-win32-x86\lib\libxml2.a" -DLIBXML2_INCLUDE_DIR="C:\libxml2\libxml2-2.9.2-win32-x86\include\libxml2" -DQt5Widgets_DIR="C:/Qt/Static/5.3.1/lib/cmake/Qt5Widgets" -DBUILD_DIST=ON -DCMAKE_INCLUDE_PATH="C:/MinGW/i686-4.9.1-release-posix-dwarf-rt_v3-rev2/mingw32/include" -DCMAKE_LIBRARY_PATH="C:/MinGW/i686-4.9.1-release-posix-dwarf-rt_v3-rev2/mingw32/lib" -DARCH="i686" -DCMAKE_C_FLAGS="-m32 -DLIBXML_STATIC" -DCMAKE_CXX_FLAGS="-m32 -DLIBXML_STATIC"
mingw32-make VERBOSE=1

:: Compilation 64 bits
:: del CMakeCache.txt
:: rmdir CMakeFiles /s
:: set PATH=C:\Program Files (x86)\CMake\bin;C:\libxml2\libxml2-2.9.2-win32-x86_64\bin;C:\Program Files\mingw-w64\x86_64-4.9.2-release-posix-seh-rt_v3-rev0\mingw64\bin;C:\Qt\5.3\mingw482_32\bin\;%PATH%
:: cmake . -G "MinGW Makefiles" -DBUILD_GUI=ON -DLIBXML2_LIBRARIES="C:\libxml2\libxml2-2.9.2-win32-x86_64\lib\libxml2.dll.a" -DLIBXML2_INCLUDE_DIR="C:\libxml2\libxml2-2.9.2-win32-x86_64\include\libxml2" -DBOOST_ROOT="C:\boost\boost_1_56_0_bin_64" -DLIBXML2_DEFINITIONS="" -DARCH=x86_64 -DCMAKE_C_FLAGS="-m64" -DCMAKE_CXX_FLAGS="-m64" -DCMAKE_BUILD_TYPE=Debug -DQt5Widgets_DIR=C:\Qt\5.3\mingw482_32\lib\cmake\Qt5Widgets
:: mingw32-make VERBOSE=1