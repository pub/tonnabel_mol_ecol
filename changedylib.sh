#!/bin/sh
set -x

#
# File changedylib.sh
# Change id and ref of libraries link in binaries and libraries
# to allow bundle distribution with QT
#


QT_PATH=`cd $2 && pwd && cd $OLDPWD`
#echo $QT_PATH
#QT_PATH=/Users/jeff/Qt5.3.2/5.3/clang_64/lib

# change QtCore id and ref
install_name_tool -id @executable_path/../../Frameworks/QtCore.framework/Versions/5/QtCore ../../Frameworks/QtCore.framework/Versions/5/QtCore
install_name_tool -change ${QT_PATH}/lib/QtCore.framework/Versions/5/QtCore @executable_path/../../Frameworks/QtCore.framework/Versions/5/QtCore MEMM_GUI
# change QtGui id and ref
install_name_tool -id @executable_path/../../Frameworks/QtGui.framework/Versions/5/QtGui ../../Frameworks/QtGui.framework/Versions/5/QtGui
install_name_tool -change ${QT_PATH}/lib/QtGui.framework/Versions/5/QtGui @executable_path/../../Frameworks/QtGui.framework/Versions/5/QtGui MEMM_GUI
# change QtWidgets id and ref
install_name_tool -id @executable_path/../../Frameworks/QtWidgets.framework/Versions/5/QtWidgets ../../Frameworks/QtWidgets.framework/Versions/5/QtWidgets
install_name_tool -change ${QT_PATH}/lib/QtWidgets.framework/Versions/5/QtWidgets @executable_path/../../Frameworks/QtWidgets.framework/Versions/5/QtWidgets MEMM_GUI

# change ref in libraries
install_name_tool -change ${QT_PATH}/lib/QtCore.framework/Versions/5/QtCore @executable_path/../../Frameworks/QtCore.framework/Versions/5/QtCore ../../Frameworks/QtGui.framework/Versions/5/QtGui
install_name_tool -change ${QT_PATH}/lib/QtCore.framework/Versions/5/QtCore @executable_path/../../Frameworks/QtCore.framework/Versions/5/QtCore ../../Frameworks/QtWidgets.framework/Versions/5/QtWidgets
install_name_tool -change ${QT_PATH}/lib/QtGui.framework/Versions/5/QtGui @executable_path/../../Frameworks/QtGui.framework/Versions/5/QtGui ../../Frameworks/QtWidgets.framework/Versions/5/QtWidgets

# Change id of plugin libqcocoa.dylib
install_name_tool -id @executable_path/../../plugins/platforms/libqcocoa.dylib ../../plugins/platforms/libqcocoa.dylib
#change ref in libqcocoa.dylib
install_name_tool -change ${QT_PATH}/lib/QtCore.framework/Versions/5/QtCore @executable_path/../../Frameworks/QtCore.framework/Versions/5/QtCore ../../plugins/platforms/libqcocoa.dylib
install_name_tool -change ${QT_PATH}/lib/QtGui.framework/Versions/5/QtGui @executable_path/../../Frameworks/QtGui.framework/Versions/5/QtGui ../../plugins/platforms/libqcocoa.dylib
install_name_tool -change ${QT_PATH}/lib/QtWidgets.framework/Versions/5/QtWidgets @executable_path/../../Frameworks/QtWidgets.framework/Versions/5/QtWidgets ../../plugins/platforms/libqcocoa.dylib

# change QtPrintSupport id 
install_name_tool -id @executable_path/../../Frameworks/QtPrintSupport.framework/Versions/5/QtPrintSupport ../../Frameworks/QtPrintSupport.framework/Versions/5/QtPrintSupport
# change QtprintSupport ref in libqcocoa.dylib
install_name_tool -change ${QT_PATH}/lib/QtPrintSupport.framework/Versions/5/QtPrintSupport @executable_path/../../Frameworks/QtPrintSupport.framework/Versions/5/QtPrintSupport ../../plugins/platforms/libqcocoa.dylib
# change in QtPrintSupport ref of libraries
install_name_tool -change ${QT_PATH}/lib/QtCore.framework/Versions/5/QtCore @executable_path/../../Frameworks/QtCore.framework/Versions/5/QtCore ../../Frameworks/QtPrintSupport.framework/Versions/5/QtPrintSupport
install_name_tool -change $QT_PATH/QtGui.framework/Versions/5/QtGui @executable_path/../../Frameworks/QtGui.framework/Versions/5/QtGui ../../Frameworks/QtPrintSupport.framework/Versions/5/QtPrintSupport
install_name_tool -change ${QT_PATH}/lib/QtWidgets.framework/Versions/5/QtWidgets @executable_path/../../Frameworks/QtWidgets.framework/Versions/5/QtWidgets ../../Frameworks/QtPrintSupport.framework/Versions/5/QtPrintSupport

