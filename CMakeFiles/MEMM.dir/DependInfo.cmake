# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/Users/eklein/1 Chercher/MEMM_Leuca/src/main.cpp" "/Users/eklein/1 Chercher/MEMM_Leuca/CMakeFiles/MEMM.dir/src/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "Clang")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "MEMM_DATA_PATH=\"/usr/local/share/memm/\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "."
  "include"
  "include/GUI"
  "/usr/local/include"
  "/Library/Developer/CommandLineTools/SDKs/MacOSX10.14.sdk/usr/include/libxml2"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/Users/eklein/1 Chercher/MEMM_Leuca/CMakeFiles/MEMMLIB.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
