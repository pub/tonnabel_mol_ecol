////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                          ////
//          RandomMatingModel v1.2                                                          ////
//          Copyright© - INRA - 2009                                                        ////
//          Klein EK, Desassis N, Oddou-Muratorio S, Carpentier F, Bonnefon O               ////
//          Licence GPL                                                                     ////
//                                                                                          ////
//          Bayesian estimation of individual fecundities and pollen dispersal kernel       ////
//          from microsatellite data on seeds and adult trees.                              ////
//                                                                                          ////
//          This code was developed by Etienne Klein, INRA Avignon, BioSP                   ////
//          The reference to this method is                                                 ////
//          Klein EK, Desassis N, Oddou-Muratorio 2008. Molecular Ecology. 17: 3323-3336    ////
//                                                                                          ////
//          Improved: Simplified Burn-in and non-burn-in loops                              ////
//          Improved: Gamma and LN options provided as objects                              ////
//          Included: Option to read distances from an external files                       ////
//                     !!!!!! New Parameters.txt files !!!!!                                ////
//          Improved: Likelihood provided as objets                                         ////
//                                                                                          ////
//                                                                                          ////
////////////////////////////////////////////////////////////////////////////////////////////////


#include <iostream>
#include <getopt.h>

#include "Parameters.hpp"
#include "ParamTXT.hpp"
#include "ParamXML.hpp"
#include "MEMM.hpp"
#include "MEMM_logLik.hpp"
#include "MEMM_seedlings.hpp"
//#include "MEMM_seedlings_2kernels.hpp"

#define CLASSIC 0
#define SEEDLINGS 1
#define SEEDLINGS_2K 2  // A vérifier

#ifndef MEMM_DATA_PATH
#define MEMM_DATA_PATH "."
#endif

using namespace std;

void usage() {
  cout <<
    "------------------------------------------------------------------------------\n" <<
    " MEMM v 2.0 by BioSP\n" <<
    "------------------------------------------------------------------------------\n" <<
    "USAGE  : MEMM [options]\n" <<
    "OPTIONS :\n" <<
    "   -h              show this message\n" <<
    "   -v              show version info\n" <<
    "   -V              set verbose mode\n" <<
    "   -p <file_in>    a parameters file\n" <<
    "   -s              run seedlings mode\n" <<
    "   -k              run seedlings mode with 2 kernels\n" <<
    "  eg. MEMM -p parameters.txt \n" <<
    "------------------------------------------------------------------------------\n";
}

int main(int argc,char ** argv)
{

  int mode = CLASSIC;
  static int verbose_flag = 0;
  int opt;
  extern char *optarg;
  char * input = NULL;
  char * extension;
  Parameters * parameters = NULL;

  static struct option long_options[] =
  {
    {"verbose", no_argument, 0, 'V'},
    {"version", no_argument, 0, 'v'},
    {"help", no_argument, 0, 'h'},
    {"parameters", required_argument, 0, 'p'},
    {"seedlings", no_argument, 0, 's'},
    {"seedlings_2k", no_argument, 0, 'k'},
    {0, 0, 0, 0}

  };
  int option_index = 0;

  while ((opt = getopt_long (argc, argv, "hvVp:sk",long_options, &option_index)) != -1)
      switch(opt)
      {
        case 'h':
            usage();
            exit(0);
        case 'V':
            verbose_flag = 1;
            cerr<<"set Verbose mode"<<endl;
            break;
        case 'v':
            cerr<<argv[0]<<" version : 2.0"<<endl;
            exit(0);
        case 'p':
            input = optarg;
            break;
        case 's':
            mode = SEEDLINGS;
            break;
        case 'k':
              mode = SEEDLINGS_2K;
              break;
       case '?':
            cerr<<"Unknown option "<<opt<<endl;
            return 1;
        default : break;
      }


  if(input!=NULL)
  {
    extension = strrchr(input,'.');

    if(extension != NULL && strcmp(extension, ".txt") == 0)
    {
      parameters = new ParamTXT(input);
    }
    else if(extension != NULL && strcmp(extension,".xml") == 0)
    {
      parameters = new ParamXML(input);
    }
    else
    {
      cerr<<"Can not open "<<input<<" file"<<endl;
    }
    
    if(parameters)
    {
      if(parameters->isOK()) cout<<"Parameters file "<< input<<" loaded"<<endl;
      else{
        cerr<<"Parameters file "<<input<<" can not be loaded"<<endl;
        delete parameters;
        parameters = NULL;
      }
    }
  }

  if(input==NULL || parameters == NULL)
  {
    cout<<"Try Local parameters file "<<DEFAULT_PARAMETERS_FILE<<endl;
    parameters = new ParamXML((char*)(string(DEFAULT_PARAMETERS_FILE)).c_str());
    if(parameters->isOK())
    {
      cout<<"OK"<<endl;
    }
    else
    {
      delete parameters;
      cerr<<"Can not load local parameters file "<<DEFAULT_PARAMETERS_FILE<<endl;
      cout<<"Try default parameters file "<< MEMM_DATA_PATH<<DEFAULT_PARAMETERS_FILE<<endl;
      parameters = new ParamXML((char*)(string(MEMM_DATA_PATH) + string(DEFAULT_PARAMETERS_FILE)).c_str());
      if(parameters->isOK())
      {
        cout<<"OK"<<endl;
      }
      else
      {
        cerr<<"Can not load default parameters file "<<DEFAULT_PARAMETERS_FILE<<endl;
        exit(0);
      }

    }
  }

  MEMM * memm;

  switch(mode)
  {
    case CLASSIC :
      memm = new MEMM_logLik();
      break;
    case SEEDLINGS :
      memm = new MEMM_seedlings();
      break;
    //case SEEDLINGS_2K :
      //    memm = new MEMM_seedlings_2kernels();
        //  break;
    default :
      usage();
      exit(0);
      break;

  }
  memm->init(parameters);
  memm->burnin();
  memm->run();

  delete parameters;
  delete memm;

  return EXIT_SUCCESS;
}

