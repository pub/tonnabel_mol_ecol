#include "MEMM_seedlings.hpp"

MEMM_seedlings::MEMM_seedlings()
{
  cout<< "MEMM : Seedlings mode"<<endl;
}

MEMM_seedlings::~MEMM_seedlings()
{
  if(pLois) delete pLois;
}

void MEMM_seedlings::mcmc(int nbIteration, int thinStep)
{

  double lastFecLogLiks, nextFecLogLiks;
  double lastFecLogLikp, nextFecLogLikp;
  double previousValue, nextValue;
  double previousValue2;
  long double previousLogLik, nextLogLik;

  int xpourCent = nbIteration/20;
  if(!xpourCent) xpourCent=1;

  lastFecLogLikp = pLoi->logLik(NFath, Vec_Fecp,GamA);
  lastFecLogLiks = pLois->logLik(NMoth, Vec_Fecs,GamAs);
  previousLogLik = logLikVerbose();

  //output start value
  if(thinStep){
    *ParamFec << "Iteration LoglikFecFem VarianceFecFem LoglikFecMale VarianceFecMale" <<endl;
    *ParamFec << "0 " << lastFecLogLiks << " " << GamAs << " " << lastFecLogLikp << " " << GamA << endl;
    ParamFec->flush();
    
    *IndivFec << "Iteration " ;
    for (int m=0; m<NMoth; m++) {*IndivFec << "Fem" << m << " ";}
    for (int p=0; p<NFath; p++) {*IndivFec << "Male" << p << " ";}
    
    *IndivFec << "0 " ;
    for (int m=0; m<NMoth; m++) {*IndivFec << Vec_Fecs[m] << " ";}
    for (int p=0; p<NFath; p++) {*IndivFec << Vec_Fecp[p] << " ";}
    *IndivFec << endl;
    IndivFec->flush();
    *ParamDisp << "Iteration Loglik DeltaSeed ShapeSeed MigSeed ScalePol ShapePol MigPol PollenInOvuleOut" <<endl;
    *ParamDisp << "0 " << previousLogLik
      << " " << Scales
      << " " << Shapes
      << " " << Migs
      << " " << Scale
      << " " << Shape
      << " " << Mig
      << " " << Imp
      <<endl;
    ParamDisp->flush();

  }

  if(nbIteration) cout << "it=1..."<< std::endl;
  for(int ite=1; ite<=nbIteration; ite++)
  {
    if (ite % xpourCent == 0) {cout << "it="<<ite << "..."<< std::endl;}

    //GamAs- fecundities distribution
    //nextValue = pow(GamAs, exp(0.2*gauss()));  // JEANNE, c'est ici qu'il faut changer les proposals
    nextValue = GamAs * exp(0.2*gauss());
      if( nextValue>mGamAs && nextValue<MGamAs )
    {
      lastFecLogLiks = pLois->logLik(NMoth, Vec_Fecs,GamAs);
      nextFecLogLiks = pLois->logLik(NMoth, Vec_Fecs, nextValue);
      if( accept() < exp(nextFecLogLiks-lastFecLogLiks) )
      {
        GamAs = nextValue;
        lastFecLogLiks = nextFecLogLiks;
        pLois->setDParam(MEMM_LOI_GAMA,GamAs);
      }
    }
    if(thinStep)*ParamFec << ite << " " << lastFecLogLiks << " " << GamAs << " ";

    //GamA (GamAp)- fecundities distribution
      // nextValue = pow(GamA, exp(0.2*gauss()));   // JEANNE, c'est ici qu'il faut changer les proposals
    nextValue = GamA * exp(0.2*gauss());
    if( nextValue>mGamA && nextValue<MGamA )
    {
      lastFecLogLikp = pLoi->logLik(NFath, Vec_Fecp,GamA);
      nextFecLogLikp = pLoi->logLik(NFath, Vec_Fecp, nextValue);
      if( accept() < exp(nextFecLogLikp-lastFecLogLikp) )
      {
        GamA = nextValue;
        lastFecLogLikp = nextFecLogLikp;
        pLoi->setDParam(MEMM_LOI_GAMA,GamA);
      }
    }
    if(thinStep)*ParamFec << lastFecLogLikp << " " << GamA << endl;

    for(int m=0 ; m<NMoth ; m++)
      {
          previousValue = Vec_Fecs[m];
          //previousValue2 = Vec_Fecp[m];
          pLois->tirage(Vec_Fecs[m]);
          //pLoi->tirage(Vec_Fecp[p]);
          nextLogLik = logLik();
          if(accept()<exp(nextLogLik-previousLogLik)) previousLogLik = nextLogLik;
          else
          {
              Vec_Fecs[m] = previousValue;
              //Vec_Fecp[m] = previousValue2;
          }
      }

    for(int p=0 ; p<NFath ; p++)
    {
      //previousValue = Vec_Fecs[p];
      previousValue2 = Vec_Fecp[p];
      //pLois->tirage(Vec_Fecs[p]);
      pLoi->tirage(Vec_Fecp[p]);
      nextLogLik = logLik();
      if(accept()<exp(nextLogLik-previousLogLik)) previousLogLik = nextLogLik;
      else
      {
        //Vec_Fecs[p] = previousValue;
        Vec_Fecp[p] = previousValue2;
      }
    }

    if(thinStep && (ite%thinStep == 0))
    {
      *IndivFec << ite << " " ;
      for (int m=0; m<NMoth; m++) {*IndivFec << Vec_Fecs[m] << " ";}
      for (int p=0; p<NFath; p++) {*IndivFec << Vec_Fecp[p] << " ";}
      *IndivFec << endl;
      IndivFec->flush();
    }

    // change Scales
    previousValue = Scales;
    Scales = previousValue*exp(0.2*gauss());
    if( Scales>mScales && Scales<MScales)
    {
      as=Scales*exp(gammln(2/Shapes)-gammln(3/Shapes));
      abs=-pow(as,-Shapes);
      nextLogLik = logLik();
      if( accept() < exp(nextLogLik-previousLogLik) ) previousLogLik = nextLogLik;
      else
      {
        Scales = previousValue;
        as=Scales*exp(gammln(2/Shapes)-gammln(3/Shapes));
        abs=-pow(as,-Shapes);
      }
    }
    else Scales = previousValue;

    //change Shapes
    previousValue = Shapes;
    Shapes = previousValue*exp(0.2*gauss());
    if( Shapes>mShapes && Shapes<MShapes)
    {
      as=Scales*exp(gammln(2/Shapes)-gammln(3/Shapes));
      abs=-pow(as,-Shapes);
      nextLogLik = logLik();
      if( accept() < exp(nextLogLik-previousLogLik) ) previousLogLik = nextLogLik;
      else{
        Shapes = previousValue;
        as=Scales*exp(gammln(2/Shapes)-gammln(3/Shapes));
        abs=-pow(as,-Shapes);
      }
    }
    else Shapes = previousValue;


    // change Migs
    previousValue = Migs;
    Migs = previousValue*exp(0.2*gauss());
    if( Migs>mMigs && Migs<MMigs)
    {
      nextLogLik = logLik();
      if( accept() < exp(nextLogLik-previousLogLik) ) previousLogLik = nextLogLik;
      else Migs = previousValue;
    }
    else Migs = previousValue;

      // change Imp
      previousValue = Imp;
      Imp = previousValue*exp(0.2*gauss());
      if( Imp>mImp && Imp<MImp)
      {
          nextLogLik = logLik();
          if( accept() < exp(nextLogLik-previousLogLik) ) previousLogLik = nextLogLik;
          else Imp = previousValue;
      }
      else Imp = previousValue;

      
    // change Scale
    previousValue = Scale;
    Scale = previousValue*exp(0.2*gauss());
    if( Scale>mScale && Scale<MScale)
    {
      ap=Scale*exp(gammln(2/Shape)-gammln(3/Shape));
      abp=-pow(ap,-Shape);
      nextLogLik = logLik();
      if( accept() < exp(nextLogLik-previousLogLik) ) previousLogLik = nextLogLik;
      else
      {
        Scale = previousValue;
        ap=Scale*exp(gammln(2/Shape)-gammln(3/Shape));
        abp=-pow(ap,-Shape);
      }
    }
    else Scale = previousValue;

    // change Shape
    previousValue = Shape;
    Shape = previousValue*exp(0.2*gauss());
    if( Shape>mShape && Shape<MShape)
    {
      ap=Scale*exp(gammln(2/Shape)-gammln(3/Shape));
      abp=-pow(ap,-Shape);
      nextLogLik = logLik();
      if( accept() < exp(nextLogLik-previousLogLik) ) previousLogLik = nextLogLik;
      else{
        Shape = previousValue;
        ap=Scale*exp(gammln(2/Shape)-gammln(3/Shape));
        abp=-pow(ap,-Shape);
      }
    }   
    else Shape = previousValue;

    // change Mig
    previousValue = Mig;
    Mig = previousValue*exp(0.2*gauss());
    if( Mig>mMig && Mig<MMig)
    {
      nextLogLik = logLik();
      if( accept() < exp(nextLogLik-previousLogLik) ) previousLogLik = nextLogLik;
      else Mig = previousValue;
    }
    else Mig = previousValue;

    *ParamDisp << ite<<" " << previousLogLik
      << " " << Scales
      << " " << Shapes
      << " " << Migs
      << " " << Scale
      << " " << Shape
      << " " << Mig
      << " " << Imp
      <<endl;
    ParamDisp->flush();

  }
    
    ifstream pourDIC("mean_Values_For_DIC.txt");
    if(pourDIC.good())
    {
        pourDIC >> previousLogLik >> Scales >> Shapes >> Migs >> Scale >> Shape >> Mig >> Imp;
        pourDIC >> lastFecLogLiks >> GamAs >> lastFecLogLikp >> GamA;
        for (int m=0; m<NMoth; m++) {pourDIC >> Vec_Fecs[m];}
        for (int p=0; p<NFath; p++) {pourDIC >> Vec_Fecp[p];}
        
        ap=Scale*exp(gammln(2/Shape)-gammln(3/Shape));
        abp=-pow(ap,-Shape);
        as=Scales*exp(gammln(2/Shapes)-gammln(3/Shapes));
        abs=-pow(as,-Shapes);

        pLois->setDParam(MEMM_LOI_GAMA,GamAs);
        pLoi->setDParam(MEMM_LOI_GAMA,GamA);

        cout << "Pour DIC, D_barre : " << previousLogLik << " " << lastFecLogLiks << " " << lastFecLogLikp << endl;
        cout << "Pour DIC, D(theta_barre) : " << logLik() << " " << pLois->logLik(NMoth, Vec_Fecs,GamAs) << " " << pLois->logLik(NMoth, Vec_Fecp,GamA) << endl;
        cout << "Pour DIC, pD = " << -2*(previousLogLik - logLik() ) << endl;
        cout << " DIC = " << -4*previousLogLik + 2*logLik() << endl;
        
    }

}


/***************************** PROTECTED ********************************/

long double MEMM_seedlings::logLik()
{

  long double liktemp = 0;
  long double pip = 0;
    long double pip1 = 0;
  long double pip2 = 0;
    long double totFecp =0;
  int pbm = 0;

  vector < vector<long double> > matp (NMoth, vector <long double> (NFath, 0.) );
  vector < vector<long double> > mats (Ns, vector <long double> (NMoth, 0.) );
  vector <long double> totp (NMoth);
  vector <long double> tots (Ns);

    for (int p=0; p<NFath; p++) totFecp+=Vec_Fecp[p];

    // Calcul de la matrice des pi (composition des nuages polliniques
  for (int m=0; m<NMoth; m++)
  {
    totp[m]=0;
    for (int p=0; p<NFath; p++)
    {
      matp[m][p]=exp(abp*pow(DistMP[m][p],Shape))*Vec_Fecp[p];
      if (isnan(matp[m][p])){matp[m][p]=0; pbm=1;}
      totp[m]+=matp[m][p];
    }
  }

    // Calcul de la matrice des phi (composition des pluies de graines
  for (int s=0; s<Ns; s++)
  {
    tots[s]=0;
    for (int m=0; m<NMoth; m++)
    {
      mats[s][m]=exp(abs*pow(DistSM[s][m],Shapes))*Vec_Fecs[m];
      if (isnan(mats[s][m])){mats[s][m]=0; pbm=1;}
      tots[s]+=mats[s][m];
    }
  }

  if (pbm==1) { cerr << " Warning: Overflow. One component of the dispersal matrix was not defined. " << endl;}

  for (int s=0; s<Ns; s++)
  {
    if( ProbMig[s]>0 && tots[s]>0)
    {
        pip=0;pip1=0;pip2=0;
      for (int m=0; m<NbMeres[s]; m++)
        pip += (ProbMeres[s][m].pere_prob)*mats[s][ProbMeres[s][m].pere_pot];
      
      for (int p=0; p<NbPeres[s]; p++)
            pip1 += (ProbPeres[s][p].pere_prob)*Vec_Fecp[ProbPeres[s][p].pere_pot];
        
      for (int m=0; m<NbCouples[s]; m++)
      {
        pip2 += (ProbCouples[s][m].couple_prob)*mats[s][ProbCouples[s][m].mere_pot]*matp[ProbCouples[s][m].mere_pot][ProbCouples[s][m].pere_pot]/totp[ProbCouples[s][m].mere_pot];
      }
      liktemp += log( ProbMig[s]*Migs*(1-Imp) + Migs*Imp*pip1/totFecp +(1-Migs)/tots[s]*(pip*Mig+(1-Mig)*pip2));
    }
  }

return liktemp;

}


long double MEMM_seedlings::logLikVerbose()
{
    
    long double liktemp = 0;
    long double pip = 0;
    long double pip1 = 0;
    long double pip2 = 0;
    long double totFecp =0;
    int pbm = 0;
    
    vector < vector<long double> > matp (NMoth, vector <long double> (NFath, 0.) );
    vector < vector<long double> > mats (Ns, vector <long double> (NMoth, 0.) );
    vector <long double> totp (NMoth);
    vector <long double> tots (Ns);
    
    std::ofstream outfile("/Users/klein/Chercher/Leucadendron/Leucadendron_Juillet_2015/logLikVerbose.txt");

    
    for (int p=0; p<NFath; p++) totFecp+=Vec_Fecp[p];
    
    for (int m=0; m<NMoth; m++)
    {
        totp[m]=0;
        for (int p=0; p<NFath; p++)
        {
            matp[m][p]=exp(abp*pow(DistMP[m][p],Shape))*Vec_Fecp[p];
            if (isnan(matp[m][p])){matp[m][p]=0; pbm=1;}
            totp[m]+=matp[m][p];
        }
    }
    
    for (int s=0; s<Ns; s++)
    {
        tots[s]=0;
        for (int m=0; m<NMoth; m++)
        {
            mats[s][m]=exp(abs*pow(DistSM[s][m],Shapes))*Vec_Fecs[m];
            if (isnan(mats[s][m])){mats[s][m]=0; pbm=1;}
            tots[s]+=mats[s][m];
        }
    }
    
    if (pbm==1) { cerr << " Warning: Overflow. One component of the dispersal matrix was not defined. " << endl;}
    
    for (int s=0; s<Ns; s++)
    {
        if( ProbMig[s]>0 && tots[s]>0)
        {
            pip=0;pip1=0;pip2=0;
            for (int m=0; m<NbMeres[s]; m++)
                pip += (ProbMeres[s][m].pere_prob)*mats[s][ProbMeres[s][m].pere_pot];
            
            for (int p=0; p<NbPeres[s]; p++)
                pip1 += (ProbPeres[s][p].pere_prob)*Vec_Fecp[ProbPeres[s][p].pere_pot];
            
            for (int m=0; m<NbCouples[s]; m++)
            {
                pip2 += (ProbCouples[s][m].couple_prob)*mats[s][ProbCouples[s][m].mere_pot]*matp[ProbCouples[s][m].mere_pot][ProbCouples[s][m].pere_pot]/totp[ProbCouples[s][m].mere_pot];
            }
            liktemp += log( ProbMig[s]*Migs*(1-Imp) + Migs*Imp*pip1/totFecp +(1-Migs)/tots[s]*(pip*Mig+(1-Mig)*pip2));
            outfile << s << "  " << ProbMig[s] << "  " << pip1/totFecp << "  " <<  pip/tots[s] << "  " << pip2/tots[s] << endl;
        }
    }
    
    outfile.close();
    return liktemp;
    
}



/*************** INIT ******************/
void MEMM_seedlings::loadSeeds(Parameters * p)
{
  int Nsl;
  string name;
  char * temp;

  if(p == NULL)return;

  cout<<endl<<"# Loading seeds... as parents"<<endl;
  allSeeds.clear();

  temp = p->getValue(MEMM_OFFSPRING_FILE_NAME);
  ifstream seeds(temp);
  if(temp) delete []temp;
  temp = p->getValue(MEMM_OFFSPRING_FILE_NUMBERS_OF_LOCUS);
  Nsl = atoi(temp);
  if(temp) delete []temp;

  Ns = 0;
  if(seeds.good())
  {
    while(seeds >> name)
    {
      cout<<name<<"...";
      allSeeds.push_back(new parent(name,seeds,0,0,Nsl));
      Ns++;
    }
    cout<<endl<<Ns<<" seeds loaded."<<endl;
    seeds.close();
  }
  else
  {
  cerr<<"#ERROR ! Can not read seeds file !"<<endl<<"Continue..."<<endl;
  }
}

void MEMM_seedlings::calculDistances(Parameters *p)
{
  MEMM::calculDistances(p);

  DistSM.resize(Ns,vector<double>(NMoth,0.));
  
  cout<< "Calcul distance seeds - mothers..."; //<<endl;
  for(int s=0; s<Ns; s++)
    for(int m=0; m<NMoth; m++)
        DistSM[s][m]=dynamic_cast<parent*>(allSeeds[s])->dist(*allParents[MothList[m]]);
}

void MEMM_seedlings::calculTransitionMatrix(Parameters *p)
{

  NbMeres.clear();
  NbMeres.resize(Ns);
    NbPeres.clear();
    NbPeres.resize(Ns);
  NbCouples.clear();
  NbCouples.resize(Ns);
  ProbMig.clear();
  ProbMig.resize(Ns);
  ProbMeres.resize(Ns);
    ProbPeres.resize(Ns);
  ProbCouples.resize(Ns);

  individu nul("nul", genotype(Nl));
  double probtemp;
  PerePot merepottemp;
  PerePot perepottemp;
  CouplePot couplepottemp;
    int NMistype=0;  // NEW EK -> Considering a Maximum Number of Mistypings

    ofstream pourReviewer("Dyades_Triades.txt");
    long double stemp1, stemp2, stemp3, stot;
    pourReviewer << "IDseedling  ProbMig  NbMeres  ProbMeres  NbPeres  ProbPeres  NbCouples  ProbCouples  PosteriorMig  PosteriorMeres  PosteriorPeres  PosteriorCouples" << endl;

    cout << endl << "Computing the Seedlings mendelian likelihoods..."  << endl;
  for (int s=0; s<Ns; s++)
  {
      stot=0;
      cout << s << "...";
      cout.flush();
    NbMeres[s]=0;

    ProbMig[s]=dynamic_cast<parent*>(allSeeds[s])->mendel(nul,nul,Nl,Na,SizeAll,AllFreq);
     
    cout << "Seedling # " << s << " (" << ProbMig[s]<< " - ";
      cout.flush();
      pourReviewer << s << " " << ProbMig[s] << " ";
      
    if (ProbMig[s]==0) {cout << "Seedling # " << s << " (" << dynamic_cast<parent*>(allSeeds[s])->getName() << ") carries an unknown allele " << endl;}

      stemp1=0;
    for (int m=0; m<NMoth; m++)
    {
        if(!useMatError)
            probtemp = dynamic_cast<parent*>(allSeeds[s])->mendel((*allParents[MothList[m]]),nul,Nl,Na,SizeAll,AllFreq);
        else
            { NMistype=0;
                probtemp = dynamic_cast<parent*>(allSeeds[s])->mendel((*allParents[MothList[m]]),nul,Nl,Na,SizeAll,AllFreq,MatError,NMistype);
            }

      if (probtemp>0 && NMistype <= NMistypeMax)
      {
        NbMeres[s]++;
        merepottemp.pere_pot=m;
        merepottemp.pere_prob=probtemp;
        ProbMeres[s].push_back(merepottemp);
          stemp1 += probtemp;
      }
    }

      cout << NbMeres[s]<< " - ";
      pourReviewer << NbMeres[s] << " " << stemp1 << " ";
      
      stemp2=0;
      for (int p=0; p<NFath; p++)
      {
          if(!useMatError)
              probtemp = dynamic_cast<parent*>(allSeeds[s])->mendel((*allParents[FathList[p]]),nul,Nl,Na,SizeAll,AllFreq);
          else
              { NMistype=0;
              probtemp = dynamic_cast<parent*>(allSeeds[s])->mendel((*allParents[FathList[p]]),nul,Nl,Na,SizeAll,AllFreq,MatError,NMistype);
              }
          
          if (probtemp>0 && NMistype <= NMistypeMax)
          {
              NbPeres[s]++;
              perepottemp.pere_pot=p;
              perepottemp.pere_prob=probtemp;
              ProbPeres[s].push_back(perepottemp);
              stemp2 += probtemp;
          }
      }

      cout << NbPeres[s]<< " - ";
      pourReviewer << NbPeres[s] << " " << stemp2 << " ";

      stemp3=0;
    for (int m=0; m<NbMeres[s]; m++)
    {
      for (int p=0; p<NbPeres[s]; p++)
      {
        
          if(!useMatError)
              probtemp = dynamic_cast<parent*>(allSeeds[s])->mendel((*allParents[MothList[ProbMeres[s][m].pere_pot]]),(*allParents[FathList[ProbPeres[s][p].pere_pot]]),Nl,Na,SizeAll,AllFreq);
          else
              { NMistype=0;
              probtemp = dynamic_cast<parent*>(allSeeds[s])->mendel((*allParents[MothList[ProbMeres[s][m].pere_pot]]),(*allParents[FathList[ProbPeres[s][p].pere_pot]]),Nl,Na,SizeAll,AllFreq,MatError,NMistype);
              }
          
        if (probtemp>0 && NMistype <= NMistypeMax)
        {
          NbCouples[s]++;
          couplepottemp.mere_pot=ProbMeres[s][m].pere_pot;
          couplepottemp.pere_pot=ProbPeres[s][p].pere_pot;
          couplepottemp.couple_prob=probtemp;
          ProbCouples[s].push_back(couplepottemp);
            stemp3 += probtemp;
        }
      }
    }
      cout << NbCouples[s]<< " )" << endl;
      pourReviewer << NbCouples[s] << " " << stemp3 << " ";
      
      pourReviewer << ProbMig[s]/(ProbMig[s]+stemp1+stemp2+stemp3) << " " << stemp1/(ProbMig[s]+stemp1+stemp2+stemp3) << " " << stemp2/(ProbMig[s]+stemp1+stemp2+stemp3) << " " << stemp3/(ProbMig[s]+stemp1+stemp2+stemp3) << " " << endl;

      if (s%50 == 0)
      {
          cout << "Seedling # " << s << " (" << dynamic_cast<parent*>(allSeeds[s])->getName() << " : " << ProbMig[s] << " --- ";
          for (int m=0; m<NbMeres[s]; m++)
              cout << " {" << dynamic_cast<parent*>(allParents[MothList[ProbMeres[s][m].pere_pot]])->getName() <<","<< ProbMeres[s][m].pere_prob <<"} -";
          cout << "---";
          for (int p=0; p<NbPeres[s]; p++)
              cout << " {" << dynamic_cast<parent*>(allParents[FathList[ProbPeres[s][p].pere_pot]])->getName() <<","<< ProbPeres[s][p].pere_prob <<"} -";
          cout << "---";
          for (int m=0; m<NbCouples[s]; m++)
              cout << " {" << dynamic_cast<parent*>(allParents[MothList[ProbCouples[s][m].mere_pot]])->getName() <<","<< dynamic_cast<parent*>(allParents[FathList[ProbCouples[s][m].pere_pot]])->getName()<< ProbCouples[s][m].couple_prob <<"} -";
          cout << endl;
      }
      
  }
  cout << endl << "Computation of the mendelian likelihoods : OK. " <<endl << endl;
    
    pourReviewer.close();

}

void MEMM_seedlings::loadPrior(Parameters *p)
{
  char *temp, *temp2, *temp3;

  MEMM::loadPrior(p);

  cout<< "Load more prior..."<<endl;
  temp = p->getValue(MEMM_GAMAS_INIT);
  GamAs = atof(temp);
  temp2 = p->getValue(MEMM_GAMAS_MIN);
  mGamAs = atof(temp2);
  temp3 = p->getValue(MEMM_GAMAS_MAX);
  MGamAs = atof(temp3);
  cout << "GamAs : "<< GamAs << " [" <<mGamAs <<"|"<< MGamAs << "]"<<endl;
  if(temp) delete []temp;
  if(temp2) delete []temp2;
  if(temp3) delete []temp3;

   // piOs
    if( strcmp("zigamma",(p->getValue(MEMM_IND_FEC_DIST))) == 0 ) {
            temp = p->getValue(MEMM_PI0S_INIT);
            Pi0s = atof(temp);
            temp2 = p->getValue(MEMM_PI0S_MIN);
            mPi0s = atof(temp2);
            temp3 = p->getValue(MEMM_PI0S_MAX);
            MPi0s = atof(temp3);
            cout << "pi0s : "<< Pi0s << " [" <<mPi0s <<"|"<< MPi0s << "]"<<endl;
            if(temp) delete []temp;
            if(temp2) delete []temp2;
            if(temp3) delete []temp3;
    }
    
  temp = p->getValue(MEMM_SCALES_INIT);
  Scales = atof(temp);
  temp2 = p->getValue(MEMM_SCALES_MIN);
  mScales = atof(temp2);
  temp3 = p->getValue(MEMM_SCALES_MAX);
  MScales = atof(temp3);
  cout << "Scales : "<< Scales << " [" <<mScales <<"|"<< MScales << "]"<<endl;
  if(temp) delete []temp;
  if(temp2) delete []temp2;
  if(temp3) delete []temp3;

  temp = p->getValue(MEMM_SHAPE_S_INIT);
  Shapes = atof(temp);
  temp2 = p->getValue(MEMM_SHAPE_S_MIN);
  mShapes = atof(temp2);
  temp3 = p->getValue(MEMM_SHAPE_S_MAX);
  MShapes = atof(temp3);
  cout << "Shapes : "<< Shapes << " [" <<mShapes <<"|"<< MShapes << "]"<<endl;
  if(temp) delete []temp;
  if(temp2) delete []temp2;
  if(temp3) delete []temp3;

  temp = p->getValue(MEMM_MIG_RATE_S_INIT);
  Migs = atof(temp);
  temp2 = p->getValue(MEMM_MIG_RATE_S_MIN);
  mMigs = atof(temp2);
  temp3 = p->getValue(MEMM_MIG_RATE_S_MAX);
  MMigs = atof(temp3);
  cout << "Migs : "<< Migs << " [" <<mMigs <<"|"<< MMigs << "]"<<endl;
  if(temp) delete []temp;
  if(temp2) delete []temp2;
  if(temp3) delete []temp3;

    temp = p->getValue(MEMM_MIG_RATE_IMP_INIT);
    Imp = atof(temp);
    temp2 = p->getValue(MEMM_MIG_RATE_IMP_MIN);
    mImp = atof(temp2);
    temp3 = p->getValue(MEMM_MIG_RATE_IMP_MAX);
    MImp = atof(temp3);
    cout << "Imp : "<< Imp << " [" <<mImp <<"|"<< MImp << "]"<<endl;
    if(temp) delete []temp;
    if(temp2) delete []temp2;
    if(temp3) delete []temp3;

    
  as=Scales*exp(gammln(2/Shapes)-gammln(3/Shapes));
  cout <<"Deltas : "<<as<<endl;
  abs=-pow(as,-Shapes);
  ap=Scale*exp(gammln(2/Shape)-gammln(3/Shape));
  cout <<"Deltap : "<<ap<<endl;
  abp=-pow(ap,-Shape);

  Vec_Fecs.resize(NMoth, 1.0 );
  Vec_Fecp.resize(NFath, 1.0 );

  cout<<"end loading prior"<<endl;

}

void MEMM_seedlings::loadParameters(Parameters *p)
{
  char * temp;
  MEMM::loadParameters(p);

//  temp = p->getValue(MEMM_IND_FEC_DIST);
//  if( strcmp("LN",temp) == 0 ) pLois = new MEMM_logNormal(gauss);
//  else pLois = new MEMM_gamma(GamAs,accept);
//  delete []temp;
    
    temp = p->getValue(MEMM_IND_FEC_DIST);
    if( strcmp("LN",temp) == 0 ) pLois = new MEMM_logNormal(gauss);
    else if( strcmp("gamma",temp) == 0 || strcmp("gama",temp) == 0) pLois = new MEMM_gamma(GamAs,accept);
    else pLois = new MEMM_zigamma(Pi0s, GamAs, accept);
    if(temp) delete []temp;

    
}

// NEW EK -> Computing posterior probabilities of parents

//void MEMM_seedlings::exportPosteriors(Parameters * p)
//{
//    double tottemp;
//    double probtemp;   // EK -> Inclus Mistyping
//    int NMistype;    // EK -> Inclus Mistyping
//    individu nul("nul", genotype(Nl));
//    
//    if(p == NULL)return;
//    
//    cout<<endl<<"# Exporting the posterior probabilities ... "<<endl;
//    cout<<endl<<"Based on ... "<< Nposterior << "  sampled MCMC steps" << endl;
//    
//    ofstream fposterior("exportProb.txt");
//    
//    if (posterior==2)
//    {   fposterior << "Seed#  SeedID  ";
//        for (int m=0; m<NPar; m++) fposterior << dynamic_cast<parent*>(allParents[m])->getName() << " ";
//        fposterior << "OUT" << endl;
//    }
//    
//    for (int s=0; s<Ns && fposterior.good(); s++)
//    {
//        
//        if (posterior==1) {
//            
//            tottemp=1;
//            //fposterior << "-----------------------------------" << endl;
//            for (int m=0; m<NbMeres[s]; m++) {
//                NMistype=0;   // EK -> Inclus Mistyping
//                probtemp = dynamic_cast<parent*>(allSeeds[s])->mendel((*allParents[ProbMeres[s][m].pere_pot]),nul,Nl,Na,SizeAll,AllFreq,MatError,NMistype); // EK -> Inclus Mistyping
//                fposterior << s << " " << dynamic_cast<parent*>(allSeeds[s])->getName() << " " << dynamic_cast<parent*>(allParents[ProbMeres[s][m].pere_pot])->getName() << " OUT ";
//                fposterior << ProbMeres[s][m].pere_prob << " " << NMistype << " " << PosteriorMeres[s][m]/Nposterior << endl;  // EK -> Inclus Mistyping
//                tottemp -= PosteriorMeres[s][m]/Nposterior;
//            }
//            
//            for (int m=0; m<NbCouples[s]; m++) {
//                NMistype=0;  // EK -> Inclus Mistyping
//                probtemp = dynamic_cast<parent*>(allSeeds[s])->mendel((*allParents[ProbCouples[s][m].mere_pot]),(*allParents[ProbCouples[s][m].pere_pot]),Nl,Na,SizeAll,AllFreq,MatError,NMistype);  // EK -> Inclus Mistyping
//                fposterior << s << " " << dynamic_cast<parent*>(allSeeds[s])->getName() << " " << dynamic_cast<parent*>(allParents[ProbCouples[s][m].mere_pot])->getName() << " ";
//                fposterior << dynamic_cast<parent*>(allParents[ProbCouples[s][m].pere_pot])->getName() << " " << ProbCouples[s][m].couple_prob << " " << NMistype << " "  <<  PosteriorCouples[s][m]/Nposterior << endl;  // EK -> Inclus Mistyping
//                tottemp -= PosteriorCouples[s][m]/Nposterior;
//            }
//            fposterior << s << " " << dynamic_cast<parent*>(allSeeds[s])->getName() << " OUT OUT " << ProbMig[s] << " 0 " << tottemp << endl; // EK -> Inclus Mistyping
//            
//        }
//        
//        else if (posterior==2)
//            
//        {
//            fposterior << s << " " << dynamic_cast<parent*>(allSeeds[s])->getName() << " ";
//            for (int m=0; m<NPar+1; m++) fposterior << PosteriorMeres[s][m]/Nposterior << " ";
//            fposterior << endl;
//            
//        }
//    }
//    
//    fposterior.close();
//    if(temp) delete []temp;
//}

