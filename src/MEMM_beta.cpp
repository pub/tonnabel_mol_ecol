#include <iostream>
#include <string>
#include "MEMM_util.h"
#include "MEMM_beta.h"


MEMM_beta::MEMM_beta(double& palpha, double& pbeta,
		        boost::variate_generator<boost::lagged_fibonacci19937&, boost::uniform_real<> >& accept):
  _palpha(palpha),_pbeta(pbeta),_accept(accept),_dbeta(palpha,pbeta)
{
  _IDLoi=MEMM_IDLOI_BETA;
    std::cout<<"Using beta distribution"<<std::endl;
}



MEMM_beta::~MEMM_beta()
{
    //dtor
}

void MEMM_beta::tirage(double& v){
  v= quantile(_dbeta,_accept());
}
double MEMM_beta::logLik(int npar, const std::vector<double> & prop, double& palpha, double& pbeta) {

  // Parametrage de la loi gamma en palpha pbeta.
  // Espérance = palpha / (palpha+pbeta)
    
    
  double liktemp=0, a1;
    a1 = -log(boost::math::beta(palpha,pbeta));
    
  for (int p=0; p<npar; p++) {
    liktemp += a1 + (palpha-1)*log(prop[p])+(pbeta-1)*log(1-prop[p]);
  }
  return liktemp;
}

double MEMM_beta::logLik(int npar, const std::vector<double> & prop, double& palpha) {}

void MEMM_beta::setDParam(unsigned int indiceParam, double & value){
  switch (indiceParam)
    {
    case MEMM_LOI_ALPHA:
            _palpha = value;
            _dbeta = boost::math::beta_distribution<>(_palpha,_pbeta);
      break;
    case MEMM_LOI_BETA:
            _pbeta = value;
            _dbeta = boost::math::beta_distribution<>(_palpha,_pbeta);
    break;
    default:
      printf("Error: MEMM_beta indiceParam out of range \n");
    }
}
