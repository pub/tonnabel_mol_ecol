#include "InputParamWidget.hpp"
//#include "moc_InputParamWidget.cpp"


InputParamWidget::InputParamWidget() : QWidget()
{
    vLayout = new QFormLayout();

    /*
     * File and data for parents
     * */
    QHBoxLayout * hLayout1;
    QPushButton * cFile1;

    hLayout1 = new QHBoxLayout();
    pLEdit1 = new QLineEdit();
    pLEdit1->setReadOnly(true);
    hLayout1->addWidget(pLEdit1);
    cFile1 = new QPushButton("File",this);
    hLayout1->addWidget(cFile1);
    QObject::connect(cFile1,SIGNAL(clicked()),this,SLOT(ChooseFileParent()));
    vLayout->addRow(new QLabel("Parents file:"),hLayout1);


    nlSpinBox = new QSpinBox();
    nlSpinBox->resize(10,nlSpinBox->height());
    QObject::connect(nlSpinBox,SIGNAL(valueChanged(int)),this,SLOT(ChangeLocusNumberParent(int)));
    vLayout->addRow(new QLabel("Numbers of locus:"),nlSpinBox);
    
    ccSpinBox = new QSpinBox();
    vLayout->addRow(new QLabel("Class covariates:"),ccSpinBox);
    QObject::connect(ccSpinBox,SIGNAL(valueChanged(int)),this,SLOT(ChangeClassCovariatesParent(int)));

    qcSpinBox = new QSpinBox();
    vLayout->addRow(new QLabel("Quantitative covariates:"),qcSpinBox);
    QObject::connect(qcSpinBox,SIGNAL(valueChanged(int)),this,SLOT(ChangeQtCovariatesParent(int)));

    wvSpinBox = new QSpinBox();
    vLayout->addRow(new QLabel("Weighting variables:"),wvSpinBox);
    QObject::connect(wvSpinBox,SIGNAL(valueChanged(int)),this,SLOT(ChangeWeightVarParent(int)));

    /*
     * File and data for offspring
     */
    QHBoxLayout * hLayout6;
    QPushButton * cFile2;

    hLayout6 = new QHBoxLayout();
    oLEdit2 = new QLineEdit();
    oLEdit2->setReadOnly(true);
    hLayout6->addWidget(oLEdit2);
    cFile2 = new QPushButton("File",this);
    hLayout6->addWidget(cFile2);
    vLayout->addRow(new QLabel("Offspring file:"),hLayout6);
    QObject::connect(cFile2,SIGNAL(clicked()),this,SLOT(ChooseFileOffspring()));


    nlSpinBox2 = new QSpinBox();
    vLayout->addRow(new QLabel("Numbers of locus:"),nlSpinBox2);
    QObject::connect(nlSpinBox2,SIGNAL(valueChanged(int)),this,SLOT(ChangeLocusNumberOffspring(int)));
    
    /*
     * File and mode for allelic frequencies
     */
    QPushButton * cFile3;
    QHBoxLayout * hLayoutAF;
    hLayoutAF = new QHBoxLayout();
    modeAF = new QComboBox();
    modeAF->addItem("file");
    modeAF->addItem("file_dist");
    hLayoutAF->addWidget(modeAF);
    afLEdit3 = new QLineEdit();
    afLEdit3->setReadOnly(true);
    hLayoutAF->addWidget(afLEdit3);
    cFile3 = new QPushButton("File",this);
    hLayoutAF->addWidget(cFile3);
    vLayout->addRow(new QLabel("Allelic frequencies file:"),hLayoutAF);
    QObject::connect(cFile3,SIGNAL(clicked()),this,SLOT(ChooseFileAF()));
    QObject::connect(modeAF,SIGNAL(currentIndexChanged(const QString&)),this,SLOT(ChangeModeAF(const QString&)));

    setLayout(vLayout);

}

InputParamWidget::InputParamWidget(ParamXML *p) : InputParamWidget()
{
    param = p;
}

InputParamWidget::~InputParamWidget(){}

void InputParamWidget::setParam(ParamXML * p)
{
    param = p;
}

void InputParamWidget::initValue()
{
    pLEdit1->setText(QString((param->getValue(INPUT_PARENTS_FILE_NAME)).c_str()));
    nlSpinBox->setValue(atoi(param->getValue(INPUT_PARENTS_FILE_NUMBERS_OF_LOCUS).c_str()));
    ccSpinBox->setValue(atoi(param->getValue(INPUT_PARENTS_FILE_CLASS_COV).c_str()));
    qcSpinBox->setValue(atoi(param->getValue(INPUT_PARENTS_FILE_QT_COV).c_str()));
    wvSpinBox->setValue(atoi(param->getValue(INPUT_PARENTS_FILE_WEIGHT_VAR).c_str()));
    
    oLEdit2->setText(QString((param->getValue(INPUT_OFFSPRING_FILE_NAME)).c_str()));
    nlSpinBox2->setValue(atoi(param->getValue(INPUT_OFFSPRING_FILE_NUMBERS_OF_LOCUS).c_str()));

    afLEdit3->setText(QString((param->getValue(INPUT_AF_FILE_NAME)).c_str()));
    modeAF->setCurrentIndex(modeAF->findText(QString((param->getValue(INPUT_AF_FILE_MODE)).c_str())));
}

void InputParamWidget::ChooseFileParent()
{
    QString filename = QFileDialog::getOpenFileName(this,tr("Open text"), "~/", tr("Text files (*.txt)"));
    if(filename != NULL){
        pLEdit1->setText(filename);
        param->setValue(INPUT_PARENTS_FILE_NAME,filename.toStdString());
    }

}

void InputParamWidget::ChangeLocusNumberParent(int v)
{
    param->setValue(INPUT_PARENTS_FILE_NUMBERS_OF_LOCUS,to_string(v));
}
void InputParamWidget::ChangeClassCovariatesParent(int v)
{
    param->setValue(INPUT_PARENTS_FILE_CLASS_COV,to_string(v));
}
void InputParamWidget::ChangeQtCovariatesParent(int v)
{
    param->setValue(INPUT_PARENTS_FILE_QT_COV,to_string(v));
}
void InputParamWidget::ChangeWeightVarParent(int v)
{
    param->setValue(INPUT_PARENTS_FILE_WEIGHT_VAR,to_string(v));
}

void InputParamWidget::ChooseFileOffspring()
{
    QString filename = QFileDialog::getOpenFileName(this,tr("Open text"), "~/", tr("Text files (*.txt)"));
    if(filename != NULL)
    {
        oLEdit2->setText(filename);
        param->setValue(INPUT_OFFSPRING_FILE_NAME,filename.toStdString());
    }

}
void InputParamWidget::ChangeLocusNumberOffspring(int v)
{
    param->setValue(INPUT_OFFSPRING_FILE_NUMBERS_OF_LOCUS,to_string(v));
}

void InputParamWidget::ChooseFileAF()
{
    QString filename = QFileDialog::getOpenFileName(this,tr("Open text"), "~/", tr("Text files (*.txt)"));
    if(filename != NULL)
    {
        afLEdit3->setText(filename);
        param->setValue(INPUT_AF_FILE_NAME,filename.toStdString());
    }
}

void InputParamWidget::ChangeModeAF(const QString& s)
{
    param->setValue(INPUT_AF_FILE_MODE,s.toStdString());
}
