/**
 * \file ParamParamWidget.cpp
 * \brief Parameters arguments editor
 * \author Jean-Francois Rey
 * \version 1.0
 * \date 04 Dec 2013
 */

#include "ParamParamWidget.hpp"

ParamParamWidget::ParamParamWidget() : QWidget()
{
    // Fill grid and add signals
    int r,c;
    vLayout = new QGridLayout();

    r=0;
    c=0;
    modeDist = new QComboBox();
    modeDist->addItem("LN");
    modeDist->addItem("Gama");
    QObject::connect(modeDist,SIGNAL(currentIndexChanged(const QString&)),this,SLOT(ChangeModeDist(const QString&)));
    vLayout->addWidget(new QLabel("Individual\nfecundities:"),r,c++);
    vLayout->addWidget(modeDist,r,c++,Qt::AlignRight);

    c=0;
    seedSpinBox = new QSpinBox();
    seedSpinBox->setMaximum(1000000);
    vLayout->addWidget(new QLabel("Seed:"),++r,c++);
    vLayout->addWidget(seedSpinBox,r,c++);
    QObject::connect(seedSpinBox,SIGNAL(valueChanged(int)),this,SLOT(ChangeSeed(int)));

    c=0;
    vLayout->addWidget(new QLabel("Gama"),++r,c++);
    vLayout->addWidget(new QLabel("Initial:"),r,c++,Qt::AlignRight);
    gamaInitSB = new QDoubleSpinBox();
    gamaInitSB->setMaximum(10000);
    vLayout->addWidget(gamaInitSB,r,c++);
    vLayout->addWidget(new QLabel("min:"),r,c++,Qt::AlignRight);
    gamaMinSB = new QDoubleSpinBox();
    vLayout->addWidget(gamaMinSB,r,c++);
    vLayout->addWidget(new QLabel("max:"),r,c++,Qt::AlignRight);
    gamaMaxSB = new QDoubleSpinBox();
    gamaMaxSB->setMaximum(10000);
    vLayout->addWidget(gamaMaxSB,r,c++);
    QObject::connect(gamaInitSB,SIGNAL(valueChanged(double)),this,SLOT(ChangeGamaInit(double)));
    QObject::connect(gamaMinSB,SIGNAL(valueChanged(double)),this,SLOT(ChangeGamaMin(double)));
    QObject::connect(gamaMaxSB,SIGNAL(valueChanged(double)),this,SLOT(ChangeGamaMax(double)));

    c=0;
    vLayout->addWidget(new QLabel("Delta"),++r,c++);
    vLayout->addWidget(new QLabel("Initial:"),r,c++,Qt::AlignRight);
    deltaInitSB = new QDoubleSpinBox();
    deltaInitSB->setMaximum(10000);
    vLayout->addWidget(deltaInitSB,r,c++);
    vLayout->addWidget(new QLabel("min:"),r,c++,Qt::AlignRight);
    deltaMinSB = new QDoubleSpinBox();
    deltaMinSB->setMaximum(10000);
    vLayout->addWidget(deltaMinSB,r,c++);
    vLayout->addWidget(new QLabel("max:"),r,c++,Qt::AlignRight);
    deltaMaxSB = new QDoubleSpinBox();
    deltaMaxSB->setMaximum(100000);
    vLayout->addWidget(deltaMaxSB,r,c++);
    QObject::connect(deltaInitSB,SIGNAL(valueChanged(double)),this,SLOT(ChangeDeltaInit(double)));
    QObject::connect(deltaMinSB,SIGNAL(valueChanged(double)),this,SLOT(ChangeDeltaMin(double)));
    QObject::connect(deltaMaxSB,SIGNAL(valueChanged(double)),this,SLOT(ChangeDeltaMax(double)));

    c=0;
    vLayout->addWidget(new QLabel("Shape b"),++r,c++);
    vLayout->addWidget(new QLabel("Initial:"),r,c++,Qt::AlignRight);
    shapeBInitSB = new QDoubleSpinBox();
    vLayout->addWidget(shapeBInitSB,r,c++);
    vLayout->addWidget(new QLabel("min:"),r,c++,Qt::AlignRight);
    shapeBMinSB = new QDoubleSpinBox();
    vLayout->addWidget(shapeBMinSB,r,c++);
    vLayout->addWidget(new QLabel("max:"),r,c++,Qt::AlignRight);
    shapeBMaxSB = new QDoubleSpinBox();
    vLayout->addWidget(shapeBMaxSB,r,c++);
    QObject::connect(shapeBInitSB,SIGNAL(valueChanged(double)),this,SLOT(ChangeShapeBInit(double)));
    QObject::connect(shapeBMinSB,SIGNAL(valueChanged(double)),this,SLOT(ChangeShapeBMin(double)));
    QObject::connect(shapeBMaxSB,SIGNAL(valueChanged(double)),this,SLOT(ChangeShapeBMax(double)));

    c=0;
    vLayout->addWidget(new QLabel("Migration rate m"),++r,c++);
    vLayout->addWidget(new QLabel("Initial:"),r,c++,Qt::AlignRight);
    migRateMInitSB = new QDoubleSpinBox();
    vLayout->addWidget(migRateMInitSB,r,c++);
    vLayout->addWidget(new QLabel("min:"),r,c++,Qt::AlignRight);
    migRateMMinSB = new QDoubleSpinBox();
    vLayout->addWidget(migRateMMinSB,r,c++);
    vLayout->addWidget(new QLabel("max:"),r,c++,Qt::AlignRight);
    migRateMMaxSB = new QDoubleSpinBox();
    vLayout->addWidget(migRateMMaxSB,r,c++);
    QObject::connect(migRateMInitSB,SIGNAL(valueChanged(double)),this,SLOT(ChangeMigRateMInit(double)));
    QObject::connect(migRateMMinSB,SIGNAL(valueChanged(double)),this,SLOT(ChangeMigRateMMin(double)));
    QObject::connect(migRateMMaxSB,SIGNAL(valueChanged(double)),this,SLOT(ChangeMigRateMMax(double)));

    c=0;
    vLayout->addWidget(new QLabel("Selfting rate s"),++r,c++);
    vLayout->addWidget(new QLabel("Initial:"),r,c++,Qt::AlignRight);
    selfRateSInitSB = new QDoubleSpinBox();
    vLayout->addWidget(selfRateSInitSB,r,c++);
    vLayout->addWidget(new QLabel("min:"),r,c++,Qt::AlignRight);
    selfRateSMinSB = new QDoubleSpinBox();
    vLayout->addWidget(selfRateSMinSB,r,c++);
    vLayout->addWidget(new QLabel("max:"),r,c++,Qt::AlignRight);
    selfRateSMaxSB = new QDoubleSpinBox();
    vLayout->addWidget(selfRateSMaxSB,r,c++);
    QObject::connect(selfRateSInitSB,SIGNAL(valueChanged(double)),this,SLOT(ChangeSelfRateSInit(double)));
    QObject::connect(selfRateSMinSB,SIGNAL(valueChanged(double)),this,SLOT(ChangeSelfRateSMin(double)));
    QObject::connect(selfRateSMaxSB,SIGNAL(valueChanged(double)),this,SLOT(ChangeSelfRateSMax(double)));

    c=0;
    vLayout->addWidget(new QLabel("Burnin:"),++r,c++);
    burnInSB = new QSpinBox();
    burnInSB->setMaximum(1000000);
    vLayout->addWidget(burnInSB,r,c++);
    QObject::connect(burnInSB,SIGNAL(valueChanged(int)),this,SLOT(ChangeBurnIn(int)));

    c=0;
    vLayout->addWidget(new QLabel("Iteration:"),++r,c++);
    iteSB = new QSpinBox();
    iteSB->setMaximum(1000000);
    vLayout->addWidget(iteSB,r,c++);
    QObject::connect(iteSB,SIGNAL(valueChanged(int)),this,SLOT(ChangeIte(int)));

    c=0;
    vLayout->addWidget(new QLabel("Thin:"),++r,c++);
    thinSB = new QSpinBox();
    thinSB->setMaximum(50000);
    vLayout->addWidget(thinSB,r,c++);
    QObject::connect(thinSB,SIGNAL(valueChanged(int)),this,SLOT(ChangeThin(int)));

    setLayout(vLayout);
}

ParamParamWidget::ParamParamWidget(ParamXML *p) : ParamParamWidget()
{
    param = p;
}

ParamParamWidget::~ParamParamWidget(){}

void ParamParamWidget::setParam(ParamXML * p)
{   
    param = p;
} 

void ParamParamWidget::initValue()
{
    // Get Value from param and set widget
    modeDist->setCurrentIndex(modeDist->findText(QString((param->getValue(PARAM_IND_FEC_MODE,PARAM_IND_FEC_MODE_ATTRIBUT)).c_str())));
    seedSpinBox->setValue(atoi(param->getValue(PARAM_SEED).c_str()));
    gamaInitSB->setValue(atof(Tools::convertPointToComma(param->getValue(PARAM_GAMA_INIT)).c_str()));
    gamaMinSB->setValue(atof(Tools::convertPointToComma(param->getValue(PARAM_GAMA_MIN)).c_str()));
    gamaMaxSB->setValue(atof(Tools::convertPointToComma(param->getValue(PARAM_GAMA_MAX)).c_str()));
    deltaInitSB->setValue(atof(Tools::convertPointToComma(param->getValue(PARAM_DELTA_INIT)).c_str()));
    deltaMinSB->setValue(atof(Tools::convertPointToComma(param->getValue(PARAM_DELTA_MIN)).c_str()));
    deltaMaxSB->setValue(atof(Tools::convertPointToComma(param->getValue(PARAM_DELTA_MAX)).c_str()));
    shapeBInitSB->setValue(atof(Tools::convertPointToComma(param->getValue(PARAM_SHAPE_B_INIT)).c_str()));
    shapeBMinSB->setValue(atof(Tools::convertPointToComma(param->getValue(PARAM_SHAPE_B_MIN)).c_str()));
    shapeBMaxSB->setValue(atof(Tools::convertPointToComma(param->getValue(PARAM_SHAPE_B_MAX)).c_str()));
    migRateMInitSB->setValue(atof(Tools::convertPointToComma(param->getValue(PARAM_MIG_RATE_M_INIT)).c_str()));
    migRateMMinSB->setValue(atof(Tools::convertPointToComma(param->getValue(PARAM_MIG_RATE_M_MIN)).c_str()));
    migRateMMaxSB->setValue(atof(Tools::convertPointToComma(param->getValue(PARAM_MIG_RATE_M_MAX)).c_str()));
    selfRateSInitSB->setValue(atof(Tools::convertPointToComma(param->getValue(PARAM_SELF_RATE_S_INIT)).c_str()));
    selfRateSMinSB->setValue(atof(Tools::convertPointToComma(param->getValue(PARAM_SELF_RATE_S_MIN)).c_str()));
    selfRateSMaxSB->setValue(atof(Tools::convertPointToComma(param->getValue(PARAM_SELF_RATE_S_MAX)).c_str()));
    burnInSB->setValue(atoi(param->getValue(PARAM_BURNIN).c_str()));
    iteSB->setValue(atoi(param->getValue(PARAM_ITE).c_str()));
    thinSB->setValue(atoi(param->getValue(PARAM_THIN).c_str()));

}

void ParamParamWidget::ChangeModeDist(const QString& s)
{
    param->setValue(PARAM_IND_FEC_MODE,s.toStdString(),PARAM_IND_FEC_MODE_ATTRIBUT);
}
void ParamParamWidget::ChangeSeed(int v)
{
    param->setValue(PARAM_SEED,to_string(v));
}
void ParamParamWidget::ChangeGamaInit(double v)
{
    param->setValue(PARAM_GAMA_INIT,Tools::convertCommaToPoint(to_string(v)));
}
void ParamParamWidget::ChangeGamaMin(double v)
{
    param->setValue(PARAM_GAMA_MIN,Tools::convertCommaToPoint(to_string(v)));
}
void ParamParamWidget::ChangeGamaMax(double v)
{
    param->setValue(PARAM_GAMA_MAX,Tools::convertCommaToPoint(to_string(v)));
}

void ParamParamWidget::ChangeDeltaInit(double v)
{
    param->setValue(PARAM_DELTA_INIT,Tools::convertCommaToPoint(to_string(v)));
}
void ParamParamWidget::ChangeDeltaMin(double v)
{
    param->setValue(PARAM_DELTA_MIN,Tools::convertCommaToPoint(to_string(v)));
}
void ParamParamWidget::ChangeDeltaMax(double v)
{
    param->setValue(PARAM_DELTA_MAX,Tools::convertCommaToPoint(to_string(v)));
}
void ParamParamWidget::ChangeShapeBInit(double v)
{
    param->setValue(PARAM_SHAPE_B_INIT,Tools::convertCommaToPoint(to_string(v)));
}
void ParamParamWidget::ChangeShapeBMin(double v)
{
    param->setValue(PARAM_SHAPE_B_MIN,Tools::convertCommaToPoint(to_string(v)));
}
void ParamParamWidget::ChangeShapeBMax(double v)
{
    param->setValue(PARAM_SHAPE_B_MAX,Tools::convertCommaToPoint(to_string(v)));
}
void ParamParamWidget::ChangeMigRateMInit(double v)
{
    param->setValue(PARAM_MIG_RATE_M_INIT,Tools::convertCommaToPoint(to_string(v)));
}
void ParamParamWidget::ChangeMigRateMMin(double v)
{
    param->setValue(PARAM_MIG_RATE_M_MIN,Tools::convertCommaToPoint(to_string(v)));
}
void ParamParamWidget::ChangeMigRateMMax(double v)
{
    param->setValue(PARAM_MIG_RATE_M_MAX,Tools::convertCommaToPoint(to_string(v)));
}

void ParamParamWidget::ChangeSelfRateSInit(double v)
{
    param->setValue(PARAM_SELF_RATE_S_INIT,Tools::convertCommaToPoint(to_string(v)));
}
void ParamParamWidget::ChangeSelfRateSMin(double v)
{
    param->setValue(PARAM_SELF_RATE_S_MIN,Tools::convertCommaToPoint(to_string(v)));
}
void ParamParamWidget::ChangeSelfRateSMax(double v)
{
    param->setValue(PARAM_SELF_RATE_S_MAX,Tools::convertCommaToPoint(to_string(v)));
}
void ParamParamWidget::ChangeBurnIn(int v)
{
    param->setValue(PARAM_BURNIN,to_string(v));
}
void ParamParamWidget::ChangeIte(int v)
{
    param->setValue(PARAM_ITE,to_string(v));
}
void ParamParamWidget::ChangeThin(int v)
{
    param->setValue(PARAM_THIN,to_string(v));
}

