/*! \file Tools.cpp
 * \brief Tools
 * \author Jean-Francois Rey
 * \date 16 Dec 2013
 */

#include <Tools.hpp>

std::string Tools::convertPointToComma(std::string str)
{
    size_t pos = str.find_last_of('.');
    if(pos!=std::string::npos) str[pos]=',';    
    return str;
}

std::string Tools::convertCommaToPoint(std::string str)
{
    size_t pos = str.find_last_of(',');
    if(pos!=std::string::npos) str[pos]='.';    
    return str;
}
