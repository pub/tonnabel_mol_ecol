#include <cstdio>
#include <QApplication>
#include <QtPlugin>
#include <iostream>
#include <getopt.h>
#include "MainWindow.hpp"
#include "ParamXML.hpp"

using namespace std;

#ifdef _WIN32
	Q_IMPORT_PLUGIN(QWindowsIntegrationPlugin);
#endif
#ifdef __linux__
  Q_IMPORT_PLUGIN(QXcbIntegrationPlugin)
#endif
//Q_IMPORT_PLUGIN(AccessibleFactory);

/**
 * Interface to acces and modify Param
 * there is no controler as in modele MVC (Model - Vue - Controler)
 * It's a simple GUI, that affect directely param (controler is integrated in Vue)
 */

void usage() {
  cout <<
  "------------------------------------------------------------------------------\n" <<
  " MEMM_GUI v 1.0 by BioSP\n" <<
  "------------------------------------------------------------------------------\n" <<
  "USAGE  : MEMM_GUI [options]\n" <<
  "OPTIONS :\n" <<
  "   -h              show this message\n" <<
  "   -v              show version info\n" <<
  "   -p <file_in>    a parameters XML file\n" <<
  "  eg. MEMM_GUI -p parameters.xml \n" <<
  "------------------------------------------------------------------------------\n";
}

int main(int argc,char ** argv)
{

  #if defined(__APPLE__) && defined(BUILD_DIST)
    QDir dir(argv[0]);
    dir.cdUp();
    dir.cdUp();
    dir.cdUp();
    dir.cd("plugins");
    QCoreApplication::setLibraryPaths(QStringList(dir.absolutePath()));
    //cerr<<QCoreApplication::libraryPaths().join(",").toUtf8().data()<<endl;
  #endif
  QApplication app(argc,argv);

  


  int opt;
  extern char *optarg;
  ParamXML * memm_param = NULL;
  char * input = NULL;

  static struct option long_options[] =
  {
    {"version", no_argument, 0, 'v'},
    {"help", no_argument, 0, 'h'},
    {"parameters", required_argument, 0, 'p'},
    {0, 0, 0, 0}
  };
  int option_index = 0;

  while ((opt = getopt_long (argc, argv, "hvp:",long_options, &option_index)) != -1)
  switch(opt)
  {
    case 'h':
      usage();
      exit(0);
    case 'v':
      cerr<<argv[0]<<" version : 1.0"<<endl;
      exit(0);
    case 'p':
      input = optarg;
    break;
    case '?':
      cerr<<"Unknown option "<<opt<<endl;
      return 1;
    default : break;
  }

  if(input) memm_param = new ParamXML(input);
  else memm_param = new ParamXML();

  MainWindow mainW(memm_param);
  mainW.show();

  return app.exec();

}

