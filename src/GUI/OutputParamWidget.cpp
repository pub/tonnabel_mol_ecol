/*!
 * \file OutputParamWidget.cpp
 * \brief QWidget containing output arguments
 * \author Jean-Francois REY
 * \version 1.0
 * \date 04 Dec 2013
 *
 * The OutputParamWidget class is a widget contianing argument editor for the output
 */

#include "OutputParamWidget.hpp"

OutputParamWidget::OutputParamWidget() : QWidget()
{
    // Main Layout
    vLayout = new QFormLayout();

    // Gama
    // Line edit and button
    QHBoxLayout * hLayout1;
    QPushButton * cFile1;
    hLayout1 = new QHBoxLayout();
    oLEdit1 = new QLineEdit();
    hLayout1->addWidget(oLEdit1);
    cFile1 = new QPushButton("File",this);
    hLayout1->addWidget(cFile1);
    vLayout->addRow(new QLabel("GamA:"),hLayout1);
    QObject::connect(cFile1,SIGNAL(clicked()),this,SLOT(ChooseGamaFile()));
    
    // Individual fecundities
    // Line edit and button
    QHBoxLayout * hLayout2;
    QPushButton * cFile2;
    hLayout2 = new QHBoxLayout();
    oLEdit2 = new QLineEdit();
    hLayout2->addWidget(oLEdit2);
    cFile2 = new QPushButton("File",this);
    hLayout2->addWidget(cFile2);
    vLayout->addRow(new QLabel("Individual fecundities:"),hLayout2);
    QObject::connect(cFile2,SIGNAL(clicked()),this,SLOT(ChooseIndFecFile()));
 
    // Dispersal parameters
    // Line edit and button
    QHBoxLayout * hLayout3;
    QPushButton * cFile3;   
    hLayout3 = new QHBoxLayout();
    oLEdit3 = new QLineEdit();
    hLayout3->addWidget(oLEdit3);
    cFile3 = new QPushButton("File",this);
    hLayout3->addWidget(cFile3);
    vLayout->addRow(new QLabel("Dispersal parameters:"),hLayout3);
    QObject::connect(cFile3,SIGNAL(clicked()),this,SLOT(ChooseDispParamFile()));
  
    setLayout(vLayout);

}

OutputParamWidget::OutputParamWidget(ParamXML *p) : OutputParamWidget()
{
    param = p;
}

OutputParamWidget::~OutputParamWidget(){}

void OutputParamWidget::setParam(ParamXML * p)
{
    param = p;
}

void OutputParamWidget::initValue()
{
    // Set edit line with value from param
    oLEdit1->setText(QString((param->getValue(OUTPUT_GAMA_FILE_NAME,OUTPUT_GAMA_FILE_NAME_ATTRIBUT)).c_str()));
    oLEdit2->setText(QString((param->getValue(OUTPUT_IND_FEC_FILE_NAME,OUTPUT_IND_FEC_FILE_NAME_ATTRIBUT)).c_str()));
    oLEdit3->setText(QString((param->getValue(OUTPUT_DISP_FILE_NAME,OUTPUT_DISP_FILE_NAME_ATTRIBUT)).c_str()));
}

void OutputParamWidget::ChooseGamaFile()
{
    QString filename = QFileDialog::getSaveFileName(this,tr("Save File"), "~/", tr("Text files (*.txt)"));
    if(filename != NULL)
    {
        oLEdit1->setText(filename);
        param->setValue(OUTPUT_GAMA_FILE_NAME,filename.toStdString(),OUTPUT_GAMA_FILE_NAME_ATTRIBUT);
    }

}
void OutputParamWidget::ChooseIndFecFile()
{
    QString filename = QFileDialog::getOpenFileName(this,tr("Save File"), "~/", tr("Text files (*.txt)"));
    if(filename != NULL)
    {
        oLEdit2->setText(filename);
        param->setValue(OUTPUT_IND_FEC_FILE_NAME,filename.toStdString(),OUTPUT_IND_FEC_FILE_NAME_ATTRIBUT);
    }

}
void OutputParamWidget::ChooseDispParamFile()
{
    QString filename = QFileDialog::getSaveFileName(this,tr("Save File"), "~/", tr("Text files (*.txt)"));
    if(filename != NULL)
    {
        oLEdit3->setText(filename);
        param->setValue(OUTPUT_DISP_FILE_NAME,filename.toStdString(),OUTPUT_DISP_FILE_NAME_ATTRIBUT);
    }
}

