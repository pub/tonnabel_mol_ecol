/**
 * \file ParamXML.cpp
 * \brief Parameters manager
 * \author Jean-Francois REY
 * \version 1.0
 * \date 04 Dec 2013
 */


#include "ParamXML.hpp"

ParamXML::ParamXML()
{
    param_xml = NULL;
    createDefaultXMLParam();
}

ParamXML::ParamXML(string filename)
{
    param_xml = NULL;
    isOK_ = loadXMLParam(filename);
}

ParamXML::ParamXML(char * filename) : Parameters(filename)
{
    param_xml = NULL;
    string st(filename);
    if(isOK_) isOK_ = loadXMLParam(st);
}

ParamXML::~ParamXML()
{
    if(param_xml != NULL) delete param_xml;
}

void ParamXML::createDefaultXMLParam()
{
    param_xml = new XMLInterface();
    param_xml->createNewDocument();

    param_xml->loadString(DEFAULT_XML_PARAM_MEMM);

}

bool ParamXML::loadXMLParam(string filename)
{
    //if(filename == NULL || filename.empty() || filename.length == 0) return -1;

    if(param_xml != NULL) delete param_xml;
    param_xml = new XMLInterface();

    try{
        param_xml->loadFile(filename);
    }
    catch(Myexception &me)
    {
        cout << "Error loading xml parameters file : " << filename << endl;
        cout << me.what() << endl;
        return 0;
    };

    param_filename.clear();
    param_filename.assign(filename);
    
    return 1;
}

string ParamXML::getValue(string name, string attribut)
{
    string result;
    if(param_xml != NULL) result.assign(param_xml->getXPathValue(name,attribut));

    return result;
}

char * ParamXML::getValue(MEMM_parameters_t valueName)
{

  string st;
  char * temp = NULL;

  switch(valueName) {
    case MEMM_PARENTS_FILE_NAME:
      st = getValue(INPUT_PARENTS_FILE_NAME);
      break;

    case MEMM_PARENTS_FILE_NUMBER_OF_LOCUS:
      st = getValue(INPUT_PARENTS_FILE_NUMBERS_OF_LOCUS);
      break;

    case MEMM_PARENTS_FILE_CLASS_COV:
      st = getValue(INPUT_PARENTS_FILE_CLASS_COV);
      break;

    case MEMM_PARENTS_FILE_QT_COV:
      st = getValue(INPUT_PARENTS_FILE_QT_COV);
      break;

    case MEMM_PARENTS_FILE_WEIGHT_VAR:
      st = getValue(INPUT_PARENTS_FILE_WEIGHT_VAR);
      break;

    case MEMM_OFFSPRING_FILE_NAME:
      st = getValue(INPUT_OFFSPRING_FILE_NAME);
      break;

    case MEMM_OFFSPRING_FILE_NUMBERS_OF_LOCUS:
      st = getValue(INPUT_OFFSPRING_FILE_NUMBERS_OF_LOCUS);
      break;

    case MEMM_AF_FILE_MODE:
      st = getValue(INPUT_AF_FILE_MODE);
      break;

    case MEMM_AF_FILE_NAME:
      st = getValue(INPUT_AF_FILE_NAME);
      break;

    case MEMM_DIST_FILE_MODE:
      st = getValue(INPUT_DIST_FILE_MODE);
      break;

    case MEMM_DIST_FILE_NAME:
      st = getValue(INPUT_DIST_FILE_NAME);
      break;

    case MEMM_LOCUS_ERROR:
      st = getValue(INPUT_MEMM_LOCUS_ERROR);
      break;

    case MEMM_LOCUS_ERROR_FILE:
      st = getValue(INPUT_MEMM_LOCUS_ERROR_FILE);
      break; 

      case MEMM_LOCUS_ERROR_NMISTYPEMAX:
            st = getValue(PARAM_LOCUS_ERROR_NMISTYPEMAX);
            break;

    case MEMM_IND_FEC_DIST:
      st = getValue(PARAM_IND_FEC_MODE,PARAM_IND_FEC_MODE_ATTRIBUT);
      break;

    case MEMM_SEED:
      st = getValue(PARAM_SEED);
      break;

    case MEMM_GAMAS_INIT:
      st = getValue(PARAM_GAMAS_INIT);
      break;

    case MEMM_GAMAS_MIN:
      st = getValue(PARAM_GAMAS_MIN);
      break;

    case MEMM_GAMAS_MAX:
      st = getValue(PARAM_GAMAS_MAX);
      break;

    case MEMM_SCALES_INIT:
      st = getValue(PARAM_SCALES_INIT);
      break;

    case MEMM_SCALES_MIN:
      st = getValue(PARAM_SCALES_MIN);
      break;

    case MEMM_SCALES_MAX:
      st = getValue(PARAM_SCALES_MAX);
      break;

    case MEMM_SCALESLDD_INIT:
      st = getValue(PARAM_SCALESLDD_INIT);
      break;

    case MEMM_SCALESLDD_MIN:
      st = getValue(PARAM_SCALESLDD_MIN);
      break;

    case MEMM_SCALESLDD_MAX:
      st = getValue(PARAM_SCALESLDD_MAX);
      break;

    case MEMM_SHAPESLDD_INIT:
      st = getValue(PARAM_SHAPESLDD_INIT);
      break;

    case MEMM_SHAPESLDD_MIN:
      st = getValue(PARAM_SHAPESLDD_MIN);
      break;

    case MEMM_SHAPESLDD_MAX:
      st = getValue(PARAM_SHAPESLDD_MAX);
      break;

    case MEMM_ALPHALDD_INIT:
      st = getValue(PARAM_ALPHALDD_INIT);
      break;

    case MEMM_ALPHALDD_MIN:
      st = getValue(PARAM_ALPHALDD_MIN);
      break;

    case MEMM_ALPHALDD_MAX:
      st = getValue(PARAM_ALPHALDD_MAX);
      break;

    case MEMM_BETALDD_INIT:
      st = getValue(PARAM_BETALDD_INIT);
      break;

    case MEMM_BETALDD_MIN:
      st = getValue(PARAM_BETALDD_MIN);
      break;

    case MEMM_BETALDD_MAX:
      st = getValue(PARAM_BETALDD_MAX);
      break;
          
    case MEMM_SHAPE_S_INIT:
      st = getValue(PARAM_SHAPE_S_INIT);
      break;

    case MEMM_SHAPE_S_MIN:
      st = getValue(PARAM_SHAPE_S_MIN);
      break;

    case MEMM_SHAPE_S_MAX:
      st = getValue(PARAM_SHAPE_S_MAX);
      break;

    case MEMM_MIG_RATE_S_INIT:
      st = getValue(PARAM_MIG_RATE_S_INIT);
      break;

    case MEMM_MIG_RATE_S_MIN:
      st = getValue(PARAM_MIG_RATE_S_MIN);
      break;

    case MEMM_MIG_RATE_S_MAX:
      st = getValue(PARAM_MIG_RATE_S_MAX);
      break;

      case MEMM_MIG_RATE_IMP_INIT:
          st = getValue(PARAM_MIG_RATE_IMP_INIT);
          break;
          
      case MEMM_MIG_RATE_IMP_MIN:
          st = getValue(PARAM_MIG_RATE_IMP_MIN);
          break;
          
      case MEMM_MIG_RATE_IMP_MAX:
          st = getValue(PARAM_MIG_RATE_IMP_MAX);
          break;

          
    case MEMM_GAMA_INIT:
      st = getValue(PARAM_GAMA_INIT);
      break;

    case MEMM_GAMA_MIN:
      st = getValue(PARAM_GAMA_MIN);
      break;

    case MEMM_GAMA_MAX:
      st = getValue(PARAM_GAMA_MAX);
      break;

	//Début modif
    case MEMM_PI0_INIT:
      st = getValue(PARAM_PI0_INIT);
      break;

    case MEMM_PI0_MIN:
      st = getValue(PARAM_PI0_MIN);
      break;

    case MEMM_PI0_MAX:
      st = getValue(PARAM_PI0_MAX);
      break;

    case MEMM_PI0S_INIT:
      st = getValue(PARAM_PI0S_INIT);
      break;

    case MEMM_PI0S_MIN:
      st = getValue(PARAM_PI0S_MIN);
      break;

    case MEMM_PI0S_MAX:
      st = getValue(PARAM_PI0S_MAX);
      break;

	//Fin modif

    case MEMM_DELTA_INIT:
      st = getValue(PARAM_DELTA_INIT);
      break;

    case MEMM_DELTA_MIN:
      st = getValue(PARAM_DELTA_MIN);
      break;

    case MEMM_DELTA_MAX:
      st = getValue(PARAM_DELTA_MAX);
      break;

    case MEMM_SHAPE_B_INIT:
      st = getValue(PARAM_SHAPE_B_INIT);
      break;

    case MEMM_SHAPE_B_MIN:
      st = getValue(PARAM_SHAPE_B_MIN);
      break;

    case MEMM_SHAPE_B_MAX:
      st = getValue(PARAM_SHAPE_B_MAX);
      break;

    case MEMM_MIG_RATE_M_INIT:
      st = getValue(PARAM_MIG_RATE_M_INIT);
      break;

    case MEMM_MIG_RATE_M_MIN:
      st = getValue(PARAM_MIG_RATE_M_MIN);
      break;

    case MEMM_MIG_RATE_M_MAX:
      st = getValue(PARAM_MIG_RATE_M_MAX);
      break;

    case MEMM_SELF_RATE_S_INIT:
      st = getValue(PARAM_SELF_RATE_S_INIT);
      break;

    case MEMM_SELF_RATE_S_MIN:
      st = getValue(PARAM_SELF_RATE_S_MIN);
      break;

    case MEMM_SELF_RATE_S_MAX:
      st = getValue(PARAM_SELF_RATE_S_MAX);
      break;

    case MEMM_BURNIN:
      st = getValue(PARAM_BURNIN);
      break;

    case MEMM_ITE:
      st = getValue(PARAM_ITE);
      break;

    case MEMM_THIN:
      st = getValue(PARAM_THIN);
      break;

    case MEMM_GAMA_FILE_NAME:
      st = getValue(OUTPUT_GAMA_FILE_NAME,OUTPUT_GAMA_FILE_NAME_ATTRIBUT);
      break;

    case MEMM_IND_FEC_FILE_NAME:
      st = getValue(OUTPUT_IND_FEC_FILE_NAME,OUTPUT_IND_FEC_FILE_NAME_ATTRIBUT);
      break;

    case MEMM_DISP_FILE_NAME:
      st = getValue(OUTPUT_DISP_FILE_NAME,OUTPUT_DISP_FILE_NAME_ATTRIBUT);
      break;


    default:
      cerr<<"Error reading "<<valueName<<" value"<<endl;
      break;
  }

  temp = new char[st.length()+1];
  strcpy(temp,st.c_str());

  return temp;

}

bool ParamXML::setValue(string name,string value, string attribut)
{
    bool res = 0;
    
    if(param_xml != NULL) res = param_xml->setXPathValue(name,value,attribut);

    return res;

}

bool ParamXML::setValue(MEMM_parameters_t valueName, char * value)
{

}

bool ParamXML::printXMLParam()
{
    if(param_xml == NULL) return -1;

    return param_xml->print();
}

bool ParamXML::save()
{
    return saveFile(param_filename);
}

bool ParamXML::saveFile(string filename)
{
    bool b;
    if(param_xml != NULL) b = param_xml->save(filename);

    if(b){ param_filename.clear(); param_filename.assign(filename);}

    return b;
}

bool ParamXML::addElement(string  expr, string name, string text)
{
    if( param_xml != NULL) return param_xml->addXPathElement(expr,name,text);

    return -1;
}

string ParamXML::getFileName()
{
    return param_filename;
}
