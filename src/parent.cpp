#include "parent.h"

/*!
 * \file parent.cpp
 * \brief Parent class
 * \author Etienne Klein
 * \author Jean-Francois Rey
 * \version 1.5
 * \date 21 April 2015
 */

parent::parent(const std::string & name, double x, double y, std::vector<int> & cov1, std::vector<double> & cov2) :
    individu(name), _x(x), _y(y), _covarquali(cov1), _covarquanti(cov2)
{
    //std::cout << "Un parent cree" << std::endl;
}

parent::parent(std::ifstream & file_in, int nquali, int nquanti, int nloc) :
    individu()
    {
    int n;
    double x;
    file_in >> _nom;
    genotype gtemp(nloc,file_in);
    _geno=gtemp;

    file_in >> _x >> _y;
    for (int k=0; k<nquali; k++) {
        file_in >>n; _covarquali.push_back(n);}
    for (int k=0; k<nquanti; k++) {
        file_in >>x; _covarquanti.push_back(x);}
}

parent::parent(const std::string & name, std::ifstream & file_in, int nquali, int nquanti, int nloc) :
    individu(name)
    {
    int n;
    double x;
    genotype gtemp(nloc,file_in);
    _geno=gtemp;

    file_in >> _x >> _y;
    for (int k=0; k<nquali; k++) {
        file_in >>n; _covarquali.push_back(n);}
    for (int k=0; k<nquanti; k++) {
        file_in >>x; _covarquanti.push_back(x);}
}

parent::~parent()
{
    //std::cout << "Un parent supprim�" << std::endl;
    _covarquali.clear();
    _covarquanti.clear();
}

double parent::dist(const parent & pere) const
{
    return (pow((this->_x - pere._x)*(this->_x - pere._x) + (this->_y - pere._y)*(this->_y - pere._y),0.5));
}


void parent::afficher() const {
    std::cout <<std::fixed << _nom << " : {"<< _x << " , "<< _y << "}" << std::endl;
    _geno.afficher();
}

/*double parent::mendel(const individu & mere , const individu & pere, int nl, std::vector<int> na,  std::vector < std::map<int,int> > sizeall, const std::vector < std::vector<double> > & freqall) const {
    double temp=1;
    for (int l=0; l<nl; l++) {
        temp*=((this->_geno).getLocus(l)).mendel1(mere.getGeno(l),pere.getGeno(l),na[l],sizeall[l],freqall[l]);
    }
    return(temp);
}*/

