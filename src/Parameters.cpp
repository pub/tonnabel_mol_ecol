#include "Parameters.hpp"

Parameters::Parameters(): isOK_(false){}

Parameters::Parameters(const char * filein) {
  ifstream file;
  file.open(filein, ifstream::in);
  if(file.good())
  {
    isOK_=true;
    parametersFileName_ = new char[strlen(filein)+1];
    strcpy(parametersFileName_,filein);
  }
  else isOK_=false;

  file.close();
}

Parameters::~Parameters()
{
  if(parametersFileName_) delete []parametersFileName_;
}

bool Parameters::isOK()
{
  return isOK_;
}
