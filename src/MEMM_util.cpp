#include <math.h>
#include <vector>
#include <fstream>
#include <iostream>
#include "MEMM_util.h"
#include <cmath>

double gammln(double xx){
  double x,y,tmp,ser;
  static double cof[6]={76.18009172947146,-86.50532032941677,
			24.01409824083091,-1.231739572450155,
			0.1208650973866179e-2,-0.5395239384953e-5};
  int j;

  y=x=xx;
  tmp=x+5.5;
  tmp -= (x+0.5)*log(tmp);
  ser=1.000000000190015;
  for (j=0;j<=5;j++) ser += cof[j]/++y;
  return -tmp+log(2.5066282746310005*ser/x);
}
///////////////////////////////////////////////////////////////////////////////////////
//          Definition des fonctions de vraisemblance utiles
///////////////////////////////////////////////////////////////////////////////////////
/*long double loglik1 (int nm, int ns, int npar, const std::vector<double> & poids, const std::vector<std::vector<double> > & distmp, const std::vector<int> & meres, const std::vector<int> & merdesc,
		     const std::vector <double > & probmig, const std::vector <double > & probself, const std::vector <int > & nbperes,
		     const std::vector<std::vector<PerePot> > & probperes,
		     const std::vector<double> & fec, double delta, double b, double mig, double self, long double& a, long double& ab) {
  //  printf("mig=%e REP_UTI\n",mig);
  std::vector < std::vector<long double> > mat (nm);
  std::vector <long double> tot (nm);
  long double bb, liktemp=0, pip=0;
  int pbm=0;

  bb = b;
  a = delta*exp(gammln(2/b)-gammln(3/b));
  ab=-pow(a,-bb);

  //log_output << " a =  " << a << " | ab = " << ab << std::endl;
  //std::cout << a << " , " << ab << " ," ;
  for (int m=0; m<nm; m++){
    mat[m].resize(npar);
    tot[m]=0;
    for (int p=0; p<meres[m]; p++){
      mat[m][p]=exp(ab*pow(distmp[m][p],b))*fec[p]*poids[p];
      if (std::isnan(mat[m][p])){mat[m][p]=0; pbm=1;}
      tot[m]+=mat[m][p];
      //printf("%Lf REP_UTI\n",tot[m]);
    }
    mat[m][meres[m]]=0;
    for (int p=meres[m]+1; p<npar; p++){
      mat[m][p]=exp(ab*pow(distmp[m][p],b))*fec[p]*poids[p];
      if (std::isnan(mat[m][p])){mat[m][p]=0; pbm=1;}
      tot[m]+=mat[m][p];
      //printf("%Lf REP_UTI\n",tot[m]);
    }}

  if (pbm==1) { std::cout << " Warning: Overflow. One component of the dispersal matrix was not defined. " << std::endl;}
  //for(int p=0; p<npar; p++) {std::cout << mat[0][p]/tot[0] << " ; "; }
  //std::cout << std::endl;

  for (int s=0; s<ns; s++){
    //std::cout << s << " : " << tot[merdesc[s]] << " ; ";
    if( probmig[s]>0 && tot[merdesc[s]]>0) {
      pip=0;
      for (int p=0; p<nbperes[s]; p++) {
	pip += (probperes[s][p].pere_prob)*mat[merdesc[s]][probperes[s][p].pere_pot];}
      //  printf("%Lf REP_UTI\n",pip);
      //      printf("%e REP_UTI\n",probmig[s]);
      //      printf("mig=%e REP_UTI\n",mig);
      //      printf("%e REP_UTI\n",probself[s]);
      liktemp += log(probmig[s]*mig+probself[s]*self+(1-self-mig)*pip/tot[merdesc[s]]);
      //      printf("%Lf REP_UTI\n",liktemp);
      //std::cout << s << " : " << liktemp << " : " << probmig[s] << " : " << tot[merdesc[s]] << " : " << pip << std::endl;
    }
    //std::cout << s << " : " << liktemp << " : " << probmig[s] << " : " << tot[merdesc[s]] << " : " << pip << std::endl;
  }

  return liktemp;
}*/
