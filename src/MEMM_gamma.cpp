#include <iostream>
#include <string>
#include "MEMM_util.h"
#include "MEMM_gamma.h"

MEMM_gamma::MEMM_gamma(double& gamA,
		        boost::variate_generator<boost::lagged_fibonacci19937&, boost::uniform_real<> >& accept):
  _accept(accept),_gamma(1./gamA,gamA)
{
  _IDLoi=MEMM_IDLOI_GAMMA;
    std::cout<<"Using gamma law"<<std::endl;
}



MEMM_gamma::~MEMM_gamma()
{
    //dtor
}

void MEMM_gamma::tirage(double& v){
  v= quantile(_gamma,_accept());
}
double MEMM_gamma::logLik(int npar, const std::vector<double> & fec, double& A) {

  // Parametrage de la loi gamma en A, B.
  // A=1/alpha; B=alpha x beta = 1. fixe
  // k=shape=1/A; theta=scale=A
  // Esperance = B; dobs/de = Var/E/E = A

  double liktemp=0, a1, a2, a3, a4;
  a1=1/A; a2=(1-A)/A; a3=log(A)/A; a4=gammln(1/A);
  for (int p=0; p<npar; p++) {
    liktemp += -fec[p]*a1 + log(fec[p])*a2 - a3 - a4;
  }
  return liktemp;
}
void MEMM_gamma::setDParam(unsigned int indiceParam, double & value){
  switch (indiceParam)
    {
    case MEMM_LOI_GAMA:
      _gamma = boost::math::gamma_distribution<>(1./value,value);
      break;
    default:
      printf("Error: MEMM_gamma indiceParam out of range \n");
    }
}
