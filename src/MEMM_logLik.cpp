/*! \file MEMM_lokLik.cpp
 * \brief MEMM_logLik class implementation
 * \author Jean-François REY
 * \vers//ion 1.0
 * \date 23 Sept 2014
 */

#include "MEMM_logLik.hpp"

MEMM_logLik::MEMM_logLik() : MEMM()
{
  cout<< "MEMM : classic mode"<<endl;
  Nm = 0;
}

MEMM_logLik::~MEMM_logLik()
{
  if(IDmere.size() != 0) IDmere.clear();
  if(Meres.size() != 0) Meres.clear();
  if(MerDesc.size() != 0) MerDesc.clear();

}


void MEMM_logLik::mcmc(int nbIteration, int thinStep)
{
  double lastFecLogLik, nextFecLogLik;
  double previousValue, nextValue;
  long double previousLogLik, nextLogLik;

  int xpourCent = nbIteration/20;
  if(!xpourCent) xpourCent=1;

  pLoi->setDParam(MEMM_LOI_GAMA,GamA);
  lastFecLogLik = pLoi->logLik(NPar, Vec_Fec,GamA);
  previousLogLik = compute();

  // output start value
  if(thinStep){
    *ParamFec << "0 " << lastFecLogLik << " " << GamA << endl;
    ParamFec->flush();
    *IndivFec << "0 " ;
    for (int p=0; p<NPar; p++) {*IndivFec << Vec_Fec[p] << " ";}
    *IndivFec << endl;
    IndivFec->flush();
    *ParamDisp << "0 " << previousLogLik
        << " " << Scale
        << " " << Shape
        << " " << Mig
        << " " << Self
        << " " << a
        << " " << ab
        <<endl;
    ParamDisp->flush();

  }


  cout << "it=1..."<< std::endl;
  for(int ite=1; ite<=nbIteration; ite++)
  {
    if (ite % xpourCent == 0) {cout << "it="<<ite << "..."<< std::endl;}

    // GamA - fecundities distribution - Effective density dobs/de
    nextValue = pow(GamA, exp(0.2*gauss()));
    if( nextValue>mGamA && nextValue<MGamA )
    {
      lastFecLogLik = pLoi->logLik(NPar, Vec_Fec,GamA);
      nextFecLogLik = pLoi->logLik(NPar, Vec_Fec, nextValue);
      //cerr<<nextFecLogLik <<" - "<<lastFecLogLik << " = "<< nextFecLogLik-lastFecLogLik<<endl; 
      if( accept() < exp(nextFecLogLik-lastFecLogLik) )
      {
        //cerr<<"New GamA "<<nextValue<<endl;
        GamA = nextValue;
        lastFecLogLik = nextFecLogLik;
        pLoi->setDParam(MEMM_LOI_GAMA,GamA);
      }
    }
    //if(thinStep && (ite % thinStep == 0))*ParamFec << ite << " " << lastFecLogLik << " " << GamA << endl;
    if(thinStep)
    {
      //cerr<< ite << " " << lastFecLogLik << " " << GamA << endl;
      *ParamFec << ite << " " << lastFecLogLik << " " << GamA << endl;
      ParamFec->flush();
    }

    // Change male fecundities
    //previousLogLik = compute();
    for(int p=0; p<NPar ; p++)
    {
      previousValue = Vec_Fec[p];
      pLoi->tirage(nextValue);
      Vec_Fec[p] = nextValue;
      nextLogLik = compute();
      if( accept() < exp(nextLogLik-previousLogLik)) previousLogLik = nextLogLik;
      else Vec_Fec[p] = previousValue;
    }

    if(thinStep && (ite % thinStep == 0))
    {
      *IndivFec << ite << " " ;
      for (int p=0; p<NPar; p++) {*IndivFec << Vec_Fec[p] << " ";}
      *IndivFec << endl;
      IndivFec->flush();
    }

    // change Scale
    previousValue = Scale;
    Scale = previousValue*exp(0.2*gauss());
    if( Scale>mScale && Scale<MScale)
    {
      a=Scale*exp(gammln(2/Shape)-gammln(3/Shape));
      ab=-pow(a,-Shape);
      nextLogLik = compute();
      if( accept() < exp(nextLogLik-previousLogLik) ) previousLogLik = nextLogLik;
      else
      {
        Scale = previousValue;
        a=Scale*exp(gammln(2/Shape)-gammln(3/Shape));
        ab=-pow(a,-Shape);
      }
    }
    else Scale = previousValue;
   
    // change Shape
    previousValue = Shape;
    Shape = previousValue*exp(0.2*gauss());
    if( Shape>mShape && Shape<MShape)
    {
      a=Scale*exp(gammln(2/Shape)-gammln(3/Shape));
      ab=-pow(a,-Shape);
      nextLogLik = compute();
      if( accept() < exp(nextLogLik-previousLogLik) ) previousLogLik = nextLogLik;
      else{
        Shape = previousValue;
        a=Scale*exp(gammln(2/Shape)-gammln(3/Shape));
        ab=-pow(a,-Shape);
      }
    }
    else Shape = previousValue;
   
    // change Mig
    previousValue = Mig;
    Mig = previousValue*exp(0.2*gauss());
    if( Mig>mMig && Mig<MMig)
    {
      nextLogLik = compute();
      if( accept() < exp(nextLogLik-previousLogLik) ) previousLogLik = nextLogLik;
      else Mig = previousValue;
    }
    else Mig = previousValue;

    // change Self
    previousValue = Self;
    Self = previousValue*exp(0.2*gauss());
    if( Self>mSelf && Self<MSelf)
    {
      nextLogLik = compute();
      if( accept() < exp(nextLogLik-previousLogLik) ) previousLogLik = nextLogLik;
      else Self = previousValue;
    }
    else Self = previousValue;

    //if(thinStep && (ite % thinStep == 0))
    if(thinStep)
    {
      *ParamDisp << ite << " " << previousLogLik
        << " " << Scale
        << " " << Shape
        << " " << Mig
        << " " << Self
        << " " << a
        << " " << ab
        <<endl;
      ParamDisp->flush();
    }
  }
  

}

/************************ PRIVATE ************************************/
long double MEMM_logLik::compute()
{
  vector < std::vector<long double> > mat (Nm);
  vector <long double> tot (Nm);
  long double bb, liktemp=0, pip=0;
  int pbm=0;

  for (int m=0; m<Nm; m++)
  {
    mat[m].resize(NPar);
    tot[m]=0;
    for (int p=0; p<Meres[m]; p++)
    {
      mat[m][p]=exp(ab*pow(DistMP[m][p],Shape))*Vec_Fec[p]*Poids[p];
      if (std::isnan(mat[m][p])){mat[m][p]=0; pbm=1;}
      tot[m]+=mat[m][p];
      //printf("%Lf REP_LOG\n",tot[m]);
    }
    mat[m][Meres[m]]=0;
    for (int p=Meres[m]+1; p<NPar; p++)
    {
      mat[m][p]=exp(ab*pow(DistMP[m][p],Shape))*Vec_Fec[p]*Poids[p];
      if (std::isnan(mat[m][p])){mat[m][p]=0; pbm=1;}
      tot[m]+=mat[m][p];
    }
  }

  if (pbm==1) { std::cout << " Warning: Overflow. One component of the dispersal matrix was not defined. " << std::endl;}

  for (int s=0; s<Ns; s++){
    //std::cout << s << " : " << tot[merdesc[s]] << " ; ";
    if( ProbMig[s]>0 && tot[MerDesc[s]]>0)
    {
      pip=0;
      for (int p=0; p<NbPeres[s]; p++)
      {
        pip += (ProbPeres[s][p].pere_prob)*mat[MerDesc[s]][ProbPeres[s][p].pere_pot];
      }
      //printf("%Lf REP_LOG\n",pip);
      //      printf("%e REP_LOG\n",ProbMig[s]);
      //      printf("mig=%e REP_LOG\n",Mig);
      //      printf("%e REP_LOG\n",ProbSelf[s]);
      liktemp += log(ProbMig[s]*Mig+ProbSelf[s]*Self+(1-Self-Mig)*pip/tot[MerDesc[s]]);
      //      printf("%Lf REP_LOG\n",liktemp);
      //std::cout << s << " : " << liktemp << " : " << probmig[s] << " : " << tot[merdesc[s]] << " : " << pip << std::endl;
    }
    //std::cout << s << " : " << liktemp << " : " << probmig[s] << " : " << tot[merdesc[s]] << " : " << pip << std::endl;
  }
  return liktemp;
}
  

/************************ PROTECTED ****************************************/

/**************** INIT **********************/

void MEMM_logLik::loadSeeds(Parameters *p)
{
  MEMM::loadSeeds(p);

  if(p == NULL) return;

  if(IDmere.size() != 0) IDmere.clear();
  if(Meres.size() != 0) Meres.clear();
  if(MerDesc.size() != 0) MerDesc.clear();

  string name;
  Nm = 0;
  if(NPar != 0 && Ns != 0)
  {
    cout<<endl<<"# Defining mothers..."<<endl;
    MerDesc.resize(Ns);
    for (int k=0; k<Ns; k++)
    {
      name = dynamic_cast <graine*>(allSeeds[k])->getMere();
      if ( (IDmere.find(name)) == IDmere.end() )
      {
        IDmere[name]=Nm;
        Meres.push_back(fatherID[name]);
        std::cout << name << "=" << IDmere[name] << "...";
        Nm++;
      }
      MerDesc[k]=IDmere[name];
    }
    cout<<endl<<"Defining mothers : OK. "<<Nm<<" mother sampled. "<<endl;
  }
  else cerr<<"can't defining mothers ...."<<endl;
}

void MEMM_logLik::calculDistances(Parameters *p)
{
  char * temp;
  bool readfile = false;

  DistMP.resize(Nm,std::vector < double > (NPar , 0.));

  temp = p->getValue(MEMM_DIST_FILE_MODE);
  if( strcmp("file_dist",temp) == 0)
  {
    delete []temp;
    temp = p->getValue(MEMM_DIST_FILE_NAME);
    std::ifstream distances(temp);
    std::string nomtemp1, nomtemp2;

    if(distances.good())
    {
      readfile = true;
      std::cout << endl << "# Loading distances from external file "<<temp<<" ..."<<endl;
      while (distances >> nomtemp1){
        distances >> nomtemp2;
        distances >> DistMP[IDmere[nomtemp1]][fatherID[nomtemp2]];
        cout<< nomtemp1<< "x"<< nomtemp2<< " -> " <<DistMP[IDmere[nomtemp1]][fatherID[nomtemp2]] << endl;
      }
      std::cout << "Loading distances : OK. " << endl;
      distances.close();
      if(temp) delete []temp;
    }
    else
    {
      readfile = false;
      cout << "ERROR can't read Distances file "<<temp<<endl;
      distances.close();
    }
  }

  if(!readfile) 
  {
    if(temp) delete []temp;
    cout << endl << "# Computing distances from x-y coordinates of parents... Mother - Father";
    for (int m=0; m<Nm; m++)
      for (int p=0; p<NPar; p++)
      {
        DistMP[m][p]=(*allParents[Meres[m]]).dist(*allParents[p]);
      }
      cout<<endl << "Computing distances : OK. " << endl;
  }
}

void MEMM_logLik::calculTransitionMatrix(Parameters *p)
{
  NbPeres.resize(Ns);
  ProbSelf.resize(Ns);
  ProbMig.resize (Ns);
  ProbPeres.resize(Ns);

  individu nul("nul", genotype(Nl) );
  double probtemp;
  PerePot perepottemp;

  cout<< endl << "# Calcul fertilization probability"<<endl;
  cout << "Computing the mendelian likelihoods..." << std::endl;
  for (int s=0; s<Ns; s++)
  {
    NbPeres[s]=0;
    if(!useMatError)
      ProbSelf[s]=dynamic_cast<graine*>(allSeeds[s])->mendel((*allParents[Meres[MerDesc[s]]]),(*allParents[Meres[MerDesc[s]]]),Nl,Na,SizeAll,AllFreq);
    else
      ProbSelf[s]=dynamic_cast<graine*>(allSeeds[s])->mendel((*allParents[Meres[MerDesc[s]]]),(*allParents[Meres[MerDesc[s]]]),Nl,Na,SizeAll,AllFreq,MatError);
      
    if (ProbSelf[s]>0) {std::cout << "Seed # " << s << " (" << dynamic_cast<graine*>(allSeeds[s])->getName() << ") is possibly issued from selfing. Likelihood = " << ProbSelf[s] << std::endl;}

    if(!useMatError)
      ProbMig[s]=dynamic_cast<graine*>(allSeeds[s])->mendel((*allParents[Meres[MerDesc[s]]]),nul,Nl,Na,SizeAll,AllFreq);
    else
      ProbMig[s]=dynamic_cast<graine*>(allSeeds[s])->mendel((*allParents[Meres[MerDesc[s]]]),nul,Nl,Na,SizeAll,AllFreq, MatError);
          
    if (ProbMig[s]==0) {std::cout << "Seed # " << s << " (" << dynamic_cast<graine*>(allSeeds[s])->getName() << ") is certainly not issued from outside or incompatible with the mother. " << std::endl;}

    for (int p=0; p<NPar; p++)
    {
      //(*allParents[p]).afficher();
      if(!useMatError)
        probtemp = dynamic_cast<graine*>(allSeeds[s])->mendel((*allParents[Meres[MerDesc[s]]]),(*allParents[p]),Nl,Na,SizeAll,AllFreq);
      else
        probtemp = dynamic_cast<graine*>(allSeeds[s])->mendel((*allParents[Meres[MerDesc[s]]]),(*allParents[p]),Nl,Na,SizeAll,AllFreq,MatError);
      //std::cout << probtemp;
      if (probtemp>0)
      {
        NbPeres[s]++;
        perepottemp.pere_pot=p;
        perepottemp.pere_prob=probtemp;
        ProbPeres[s].push_back(perepottemp);
      }
    }

      //std::cout << "Seed #" << s << " Probmig =" << ProbMig[s] << "; Probself =" << ProbSelf[s] << "; NbPeres =" << NbPeres[s]<< std::endl;
  }
  std::cout << std::endl << "Computation of the mendelian likelihoods : OK. " <<std::endl << std::endl;

  //std::cout << "Seed #1: " << NbPeres[1] << " , " << MerDesc[1] << " , " << ProbMig[1] << " ," << ProbSelf[1] << " , " << ProbPeres[1][0].pere_pot << " , " << ProbPeres[1][0].pere_prob << std::endl;

  //for (int p=0; p<NPar; p++) {std::cout << DistMP[MerDesc[1]][p] << " ; ";}

}

void MEMM_logLik::loadPrior(Parameters *p)
{
  MEMM::loadPrior(p);

  a=Scale*exp(gammln(2/Shape)-gammln(3/Shape));
  cout <<"Delta : "<<a<<endl;
  ab=-pow(a,-Shape);

  Vec_Fec.resize(NPar, 1.0 );

}
