#include "genotype.h"

/*!
 * \file genotype.hpp
 * \brief Genotype class
 * \author Etienne Klein
 * \author Jean-Francois Rey
 * \version 1.5
 * \date 21 April 2015
 */

genotype::genotype(int nloc, std::ifstream & file_in)
{
    int n1;
    int n2;
    for (int k=0; k<nloc; k++){
        file_in >> n1; file_in >> n2;
        _genonuc.push_back(new locus(n1,n2));
    }
}

genotype::genotype(int nloc)
   {
       for (int k=0; k<nloc; k++){
        _genonuc.push_back(new locus(-1,-1));
    }
}



genotype::~genotype()
{
    /*for(long unsigned int i=0; i<_genonuc.size();i++)
      delete _genonuc[i];*/
    _genonuc.clear();
}

void genotype::afficher() const {
    for (long unsigned int k=0; k<_genonuc.size(); k++)
    {
        _genonuc[k]->afficher();
    }
        std::cout << std::endl;

}

/*double genotype::mendel(const genotype & mere, const genotype & pere, int nl, const std::vector<int> & na, const std::vector<std::map<int,int> > & sizeall, const std::vector<std::vector<double> > & freqall) const {
    double temp=1;
    for (int l=0; l<nl; l++) {
    temp *= this->_genonuc[l].mendel1(mere._genonuc[l],pere._genonuc[l],na[l], sizeall[l], freqall[l]);}
    return(temp);
}
*/

double genotype::mendel(const genotype & mere, const genotype & pere, int nl, const std::vector<int> & na, const std::vector<std::map<int,int> > & sizeall, const std::vector<std::vector<double> > & freqall, const std::vector < std::vector < std::vector <double> > > & materror) const {
    double temp=1;
    if((unsigned int)nl == _genonuc.size())
    {
      if(!materror.empty())
      {
        for (int l=0; l<nl; l++)
          temp *= this->_genonuc[l]->mendel1(*mere._genonuc[l],*pere._genonuc[l],na[l], sizeall[l], freqall[l], materror[l]);
      }
      else
      {
        for (int l=0; l<nl; l++)
          temp *= this->_genonuc[l]->mendel1(*mere._genonuc[l],*pere._genonuc[l],na[l], sizeall[l], freqall[l]);
      }
    }
    else cerr<<"ERROR : locus numbers differ "<<nl<<" vs "<<_genonuc.size()<<endl;

    return(temp);
}
