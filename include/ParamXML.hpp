#ifndef __PARAM_XML_HPP__
#define __PARAM_XML_HPP__

/*!
 * \file ParamXML.hpp
 * \brief XML Parameters manager
 * \author Jean-Francois REY
 * \version 1.0
 * \date 04 Dec 2013
 *
 * The Param class manage parameters from arguments and XML file (for MEMM project).
 * TODO implement arguments managing.
 */
#include <string>
#include <iostream>
#include <typeinfo>
#include "XMLInterface.hh"
#include "Parameters.hpp"

using namespace std;

/*! \def DEFAULT_XML_PARAM_MEMM
 * Constant containing default parameters/settings as XML.
 */
#define DEFAULT_XML_PARAM_MEMM "<?xml version='1.0' encoding='UTF-8'?>\
<document>\
<MEMM>\
    <input>\
        <!-- file containing information about parents -->\
        <file description='parents'>\
            <filename>AlisierPar.txt</filename>\
            <numbers_of_locus>6</numbers_of_locus>\
            <class_covariates>6</class_covariates>\
            <quantitative_covariables>2</quantitative_covariables>\
            <weighting_variables>1</weighting_variables>\
        </file>\
        <!-- file containing information about offspring -->\
        <file description='offspring'>\
            <filename>AlisierDesc.txt</filename>\
            <numbers_of_locus>6</numbers_of_locus>\
        </file>\
        <!-- mode of computation of AF and file containing AF -->\
        <AF enable='true'>\
            <mode>file</mode>\
            <value>freqall.txt</value>\
        </AF>\
        <AF mode='file_dist' value='distfile.txt' enable='false' />\
        <LE enable='false'>\
          <filename>LocusError.txt</filename>\
        </LE>\
    </input>\
    <parameters>\
        <!-- individual fecundities distribution -->\
        <individual_fecundities distribution='LN' />\
        <seed>12345</seed>\
        <!-- GamA dobs/dep -->\
        <gama>\
            <initial>2.0</initial>\
            <min>1.0</min>\
            <max>1000.0</max>\
        </gama>\
        <!-- mean dispersal distance delta -->\
        <delta>\
            <initial>100.0</initial>\
            <min>0.0</min>\
            <max>10000.0</max>\
        </delta>\
        <!-- shape b -->\
        <b>\
            <init>1.0</init>\
            <min>0.1</min>\
            <max>10.0</max>\
        </b>\
        <!-- migration rate m -->\
        <m>\
            <init>0.5</init>\
            <min>0.1</min>\
            <max>1.0</max>\
        </m>\
        <!-- selfting rate s -->\
        <s>\
            <init>0.05</init>\
            <min>0.0</min>\
            <max>0.1</max>\
        </s>\
        <!-- burn-in iteration -->\
        <burnin>5000</burnin>\
        <ite>50000</ite>\
        <thin>20</thin>\
    </parameters>\
    <!-- Output file -->\
    <output>\
        <!-- dobs/dep -->\
        <file type='GamA' name='ParamFec.txt' />\
        <!-- Individual fecundities -->\
        <file type='IndFec' name='IndivFec.txt' />\
        <!-- Dispersal parameters -->\
        <file type='Disp' name='ParamDisp.txt' />\
    </output>\
</MEMM>\
</document>"

/*! \defgroup XMLVariables XPath acces XML variables
 * @{
 */
#define INPUT_PARENTS_FILE_NAME "//MEMM/input/file[@description='parents']/filename"
#define INPUT_PARENTS_FILE_NUMBERS_OF_LOCUS "//MEMM/input/file[@description='parents']/numbers_of_locus"
#define INPUT_PARENTS_FILE_CLASS_COV "//MEMM/input/file[@description='parents']/class_covariates"
#define INPUT_PARENTS_FILE_QT_COV "//MEMM/input/file[@description='parents']/quantitative_covariables"
#define INPUT_PARENTS_FILE_WEIGHT_VAR "//MEMM/input/file[@description='parents']/weighting_variables"
#define INPUT_OFFSPRING_FILE_NAME "//MEMM/input/file[@description='offspring']/filename"
#define INPUT_OFFSPRING_FILE_NUMBERS_OF_LOCUS "//MEMM/input/file[@description='offspring']/numbers_of_locus"
#define INPUT_AF_FILE_MODE "//MEMM/input/AF[@enable='true']/mode"
#define INPUT_AF_FILE_NAME "//MEMM/input/AF[@enable='true']/value"
#define INPUT_DIST_FILE_MODE "//MEMM/input/Dist[@enable='true']/mode"
#define INPUT_DIST_FILE_NAME "//MEMM/input/Dist[@enable='true']/value"
#define INPUT_MEMM_LOCUS_ERROR "//MEMM/input/LE[@enable='true']"
#define INPUT_MEMM_LOCUS_ERROR_FILE "//MEMM/input/LE[@enable='true']/filename"
#define PARAM_LOCUS_ERROR_NMISTYPEMAX "//MEMM/input/LE[@enable='true']/nmistypemax"

#define PARAM_IND_FEC_MODE "//MEMM/parameters/individual_fecundities"
#define PARAM_IND_FEC_MODE_ATTRIBUT "distribution"
#define PARAM_SEED "//MEMM/parameters/seed"
#define PARAM_GAMAS_INIT "//MEMM/parameters/gamas/init"
#define PARAM_GAMAS_MIN "//MEMM/parameters/gamas/min"
#define PARAM_GAMAS_MAX "//MEMM/parameters/gamas/max"
#define PARAM_SCALES_INIT "//MEMM/parameters/scales/init"
#define PARAM_SCALES_MIN "//MEMM/parameters/scales/min"
#define PARAM_SCALES_MAX "//MEMM/parameters/scales/max"


#define PARAM_SCALESLDD_INIT "//MEMM/parameters/scalesLDD/init"
#define PARAM_SCALESLDD_MIN "//MEMM/parameters/scalesLDD/min"
#define PARAM_SCALESLDD_MAX "//MEMM/parameters/scalesLDD/max"
#define PARAM_SHAPESLDD_INIT "//MEMM/parameters/shapesLDD/init"
#define PARAM_SHAPESLDD_MIN "//MEMM/parameters/shapesLDD/min"
#define PARAM_SHAPESLDD_MAX "//MEMM/parameters/shapesLDD/max"
#define PARAM_ALPHALDD_INIT "//MEMM/parameters/alphaLDD/init"
#define PARAM_ALPHALDD_MIN "//MEMM/parameters/alphaLDD/min"
#define PARAM_ALPHALDD_MAX "//MEMM/parameters/alphaLDD/max"
#define PARAM_BETALDD_INIT "//MEMM/parameters/betaLDD/init"
#define PARAM_BETALDD_MIN "//MEMM/parameters/betaLDD/min"
#define PARAM_BETALDD_MAX "//MEMM/parameters/betaLDD/max"

// Début modif
#define PARAM_PI0_INIT "//MEMM/parameters/Pi0/init"
#define PARAM_PI0_MIN "//MEMM/parameters/Pi0/min"
#define PARAM_PI0_MAX "//MEMM/parameters/Pi0/max"
#define PARAM_PI0S_INIT "//MEMM/parameters/Pi0s/init"
#define PARAM_PI0S_MIN "//MEMM/parameters/Pi0s/min"
#define PARAM_PI0S_MAX "//MEMM/parameters/Pi0s/max"
//Fin modif

#define PARAM_SHAPE_S_INIT "//MEMM/parameters/shapes/init"
#define PARAM_SHAPE_S_MIN "//MEMM/parameters/shapes/min"
#define PARAM_SHAPE_S_MAX "//MEMM/parameters/shapes/max"
#define PARAM_MIG_RATE_S_INIT "//MEMM/parameters/migs/init"
#define PARAM_MIG_RATE_S_MIN "//MEMM/parameters/migs/min"
#define PARAM_MIG_RATE_S_MAX "//MEMM/parameters/migs/max"

#define PARAM_MIG_RATE_IMP_INIT "//MEMM/parameters/imp/init"
#define PARAM_MIG_RATE_IMP_MIN "//MEMM/parameters/imp/min"
#define PARAM_MIG_RATE_IMP_MAX "//MEMM/parameters/imp/max"

#define PARAM_GAMA_INIT "//MEMM/parameters/gama/init"
#define PARAM_GAMA_MIN "//MEMM/parameters/gama/min"
#define PARAM_GAMA_MAX "//MEMM/parameters/gama/max"
#define PARAM_DELTA_INIT "//MEMM/parameters/delta/init"
#define PARAM_DELTA_MIN "//MEMM/parameters/delta/min"
#define PARAM_DELTA_MAX "//MEMM/parameters/delta/max"
#define PARAM_SHAPE_B_INIT "//MEMM/parameters/b/init"
#define PARAM_SHAPE_B_MIN "//MEMM/parameters/b/min"
#define PARAM_SHAPE_B_MAX "//MEMM/parameters/b/max"
#define PARAM_MIG_RATE_M_INIT "//MEMM/parameters/m/init"
#define PARAM_MIG_RATE_M_MIN "//MEMM/parameters/m/min"
#define PARAM_MIG_RATE_M_MAX "//MEMM/parameters/m/max"
#define PARAM_SELF_RATE_S_INIT "//MEMM/parameters/s/init"
#define PARAM_SELF_RATE_S_MIN "//MEMM/parameters/s/min"
#define PARAM_SELF_RATE_S_MAX "//MEMM/parameters/s/max"
#define PARAM_BURNIN "//MEMM/parameters/burnin"
#define PARAM_ITE "//MEMM/parameters/ite"
#define PARAM_THIN "//MEMM/parameters/thin"

#define OUTPUT_GAMA_FILE_NAME "//MEMM/output/file[@type='GamA']"
#define OUTPUT_GAMA_FILE_NAME_ATTRIBUT "name"
#define OUTPUT_IND_FEC_FILE_NAME "//MEMM/output/file[@type='IndFec']"
#define OUTPUT_IND_FEC_FILE_NAME_ATTRIBUT "name"
#define OUTPUT_DISP_FILE_NAME "//MEMM/output/file[@type='Disp']"
#define OUTPUT_DISP_FILE_NAME_ATTRIBUT "name"
/*!@}*/


/*! \class ParamXML
 * \brief Parameters manager
 *
 * This class manage parameters as arguments or as XML file.
 */
class ParamXML : public Parameters {

    private :
        XMLInterface * param_xml; ///< XML tools
        string param_filename; ///< paramter file name

    public :
        /*!
         * \brief Constructor
         *
         * ParamXML class constructor
         */
        ParamXML();

        /*! \brief Constructor
         * \param filename : parameters XML file to open
         *
         *  ParamXML class constructor
         */
        ParamXML(string filename);
        ParamXML(char * filename);

        /*! \brief Destructor
         *
         *  ParamXML class destructor
         */
        ~ParamXML();

        /*! \brief Create and load default parameters as XML format
         *
         * Create new xml document and load DEFAULT_XML_PARAM_MEMM as default parameters
         */
        void createDefaultXMLParam();

        /*! \brief set verbose mode
         * \param b : true active verbose
         */
        void setVerbose(bool b);

        /*! \brief Load an xml parameters file
         * \param filename : xml parameters file
         * \return 1 if ok
         *
         */
        bool loadXMLParam(string filename);

        /*! \brief Print xml file in stdout
         */
        bool printXMLParam();

        /*! \brief Print help message
         *
         * Print help message for parameters
         */
        void printHelpMessage();

        /*! \brief Add an element and value in xml format
         * \param expr : XPath expression
         * \param name : element name to add
         * \param text : element value, default ""
         * \return 1 if OK otherwise return value < 1
         *
         * Add an element name with value text in a xml node represented as XPath expression.
         *
         */
        bool addElement(string expr, string name, string text="");

        /*! \brief Get value of a parameter
         * \param expr : XPath expression
         * \param attribut : element attribut name of the value to return
         * \return the value as a string.
         *
         * Get the value of an element or an element attribut value.
         */
        string getValue(string expr, string attribut="");

        virtual char * getValue(MEMM_parameters_t valueName);

        /*! \brief Set value of a parameter
         * \param expr : XPath expression
         * \param value : parameter value to set
         * \param attribut : parameter attribut to set value, if none ""
         *
         * Set the value of an element or an element attribut value.
         */
        bool setValue(string expr,string value, string attribut="");

        virtual bool setValue(MEMM_parameters_t valueName, char * value);

        /*! \brief Save parameters
         * \return 1 if OK
         *
         * Save parameters as XML file if already specified.
         */
        bool save();

        /*! \brief Save parameters in file
         * \param filename : file name to save parameters
         * \return 1 if OK
         * 
         * Save parameters as XML file if already specified.
         */
        bool saveFile(string filename);

        /*! \brief Get filename
         * \return filename as string
         *
         * Get the filename of the parameters record if any.
         */
        string getFileName();
};



#endif


