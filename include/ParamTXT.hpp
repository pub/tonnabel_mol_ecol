#ifndef __MEMM_PARAMTXT_HPP__
#define __MEMM_PARAMTXT_HPP__

/*!
 * \file ParamTXT.hpp
 * \brief TXT Parameters manager
 * \author Jean-Francois REY
 * \version 1.0
 * \date 04 Dec 2013
 *
 * The Param class manage parameters from arguments and XML file (for MEMM project).
 * Deprecated
 */


#include <iostream>
#include <fstream>
#include <cstring>
#include <limits>

#include "Parameters.hpp"

/*! \class ParamTXT
 * \brief Parameters manager for txt file
 *
 * This class manage parameters as arguments or as TXT file.
 * Deprecated ! See ParamXML
 */
class ParamTXT : public Parameters {
  public :
    ParamTXT();
    ParamTXT(char * filein);
    ~ParamTXT();
    //template<typename Type> Type getValue(MEMM_parameters_t valueName);
    virtual char * getValue(MEMM_parameters_t valueName);
    virtual bool setValue(MEMM_parameters_t valueName, char * value);


  private : 
    ifstream paramFileStream;

    void goToLine(int lineNumber);
};


#endif
