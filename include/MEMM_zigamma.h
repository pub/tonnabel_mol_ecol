#ifndef MEMM_ZIGAMMA_H
#define MEMM_ZIGAMMA_H
#include <fstream>
#include <vector>
#include <boost/random/variate_generator.hpp>
#include <boost/random/lagged_fibonacci.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/math/distributions/gamma.hpp>
#include <boost/math/special_functions/gamma.hpp>
#include "MEMM_loi.h"
/** 
 * \brief Cette classe implemente la loi gamma.
 */
class MEMM_zigamma : public MEMM_loi
{
 private:
    double _pi0, _gamA;
  boost::math::gamma_distribution<> _gamma;
  boost::variate_generator<boost::lagged_fibonacci19937&, boost::uniform_real<> >& _accept;
 public:
  /** Constructeur
      \param [gamA] permentant de construire la loi zero - inflated gamma.
   */
  MEMM_zigamma(double& pi0, double& gamA,
	     boost::variate_generator<boost::lagged_fibonacci19937&, boost::uniform_real<> >& accept);
  /** faire un tirage
      \param [out] valeur du tirage.
  */
  virtual void tirage(double& v);
  /** Calcul la vraisemblance de l'echantillon ech avec le parametre de loi param
   * \param [in] npar, taille de l'echantillon
   * \param [in] ech, echantillon
   * \param [in] deux parametres de loi
   * \return la vraisemblance
   */
  virtual double logLik(int npar,const std::vector<double> & ech,double& pi,double& A);
    virtual double logLik(int npar,const std::vector<double> & ech,double& A) {};
    /** Modifier le parametre d'indice indiceParam.
      \param [in] indice du parametre (MEMM_LOI_GAMA pour gamma(1/v,v)).
      \param [in] valeur a prendre en compte.
   */
  virtual void setDParam(unsigned int indiceParam, double & value);
  virtual ~MEMM_zigamma();
};

#endif // MEMM_ZIGAMMA_H
