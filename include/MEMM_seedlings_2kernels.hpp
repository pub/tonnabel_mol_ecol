#ifndef __MEMM_SEEDLINGS_2KERNELS_HPP__
#define __MEMM_SEEDLINGS_2KERNELS_HPP__

#include "MEMM.hpp"
#include "MEMM_seedlings.hpp"
#include "MEMM_beta.h"
#include "MEMM_zigamma.h"

/*! \class MEMM_seedlings_2kernels
 * \brief MEMM_seedlings_2kernels class
 *
 * Mother and Father are unknow.
 * Two kernels fort SDD and LDD are mixed with variable weights
 *
 */
class MEMM_seedlings_2kernels : public MEMM_seedlings {

  public :

    /*! \brief Constructor
     */
    MEMM_seedlings_2kernels();

    /*! \brief destructor
     */
    virtual ~MEMM_seedlings_2kernels();

    virtual void mcmc(int nbIteration, int thinStep);
  
  protected :

    void loadPrior(Parameters * p);

    /*! \brief Calcul model log likelihood
     */
    long double logLik();
  
    
private :

    double ScalesLDD;  //!< LDD scales parameter initial, dimension of distance (as);
    double mScalesLDD; //!< min LDD scales value
    double MScalesLDD; //!< max LDD scales value
    double ShapesLDD;  //!< LDD shapes parameter initial value, dispersal kernel (bs)
    double mShapesLDD; //!< min LDD shapes value
    double MShapesLDD; //!< max LDD shapes value

    double asLDD, absLDD; // A VERIFIER
    double KnormLDD, Knorm; // A VERIFIER
  
    
    double AlphaLDD;  //!< Beta-distribution for LDD: alpha parameter initial value
    double mAlphaLDD; //!< Beta-distribution for LDD: min alpha value
    double MAlphaLDD; //!< Beta-distribution for LDD: max alpha value
    double BetaLDD;  //!< Beta-distribution for LDD: beta parameter initial value
    double mBetaLDD; //!< Beta-distribution for LDD: min beta value
    double MBetaLDD; //!< Beta-distribution for LDD: max beta value
    
    vector<double> Vec_FreqLDD;  //!< frequency of long distance dispersal
    
    MEMM_beta * pLoiLDD; //!< distribution for individual frequencies of LDD


};

#endif

