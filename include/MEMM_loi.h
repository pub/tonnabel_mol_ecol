#ifndef MEMM_LOI_H
#define MEMM_LOI_H
#include <vector>

/** 
 * \brief Le but de cette classe est d'etre surchargee par des lois specifiques.
 */
class MEMM_loi
{
    public:
  /** Constructeur
   */
    int _IDLoi;
    
    MEMM_loi();
  /** faire un tirage
      \param [out] valeur du tirage.
   */
  virtual void tirage(double& v) =0;
  /** Calcul la vraisemblance de l'echantillon ech avec le parametre de loi param
   * \param [in] npar, taille de l'echantillon
   * \param [in] ech, echantillon
   * \param [in] parametre de loi
   * \return la vraisemblance
   */
   virtual double logLik(int npar,const std::vector<double> & ech,double& param,double& param2) {};
  virtual double logLik(int npar,const std::vector<double> & ech,double& param)=0;
  /** Modifier le parametre d'indice indiceParam.
   *   \param [in] indice du parametre
   *   \param [in] valeur a prendre en compte.
   */
  virtual void setDParam(unsigned int indiceParam, double & value) = 0;
  /** Destructeur
   */
  virtual ~MEMM_loi();
};

enum MEMM_loiCst{
  MEMM_LOI_GAMA,
    MEMM_LOI_ALPHA,
    MEMM_LOI_BETA,
    MEMM_LOI_ZIPROB

};

enum MEMM_collecLois{
    MEMM_IDLOI_LN,
    MEMM_IDLOI_GAMMA,
    MEMM_IDLOI_BETA,
    MEMM_IDLOI_ZIGAMMA
};

#endif // GENOTYPE_H
