#ifndef __MAIN_WINDOW_HPP__
#define __MAIN_WINDOW_HPP__

/*!
 * \file MainWindow.hpp
 * \brief Main Window GUI class
 * \author Jean-Francois REY
 * \version 1.0
 * \date 04 Dec 2013
 */

#include <QApplication>
#include <QMainWindow>
#include <QWidget>
#include <QObject>
#include <QMenu>
#include <QMenuBar>
#include <QToolBar>
#include <QAction>
#include <QMessageBox>
#include <QFileDialog>
#include <QFileInfo>
#include "InputParamWidget.hpp"
#include "ParamParamWidget.hpp"
#include "OutputParamWidget.hpp"
#include "ParamXML.hpp"

/*! \class MainWindow
 * \brief Main Window GUI class
 *
 * The main MEMM window GUI to modify parameters.<br>
 * Menu + ToolBar<br>
 * Include three tabs : <br>
 * - InputParamWidget
 * - ParamParamWidget
 * - OutputParamWidget
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT
    public :

        /*! \brief Constructor
         * \param p : pointer on ParamXML class
         *
         * MainWindow constructor
         */
        MainWindow(ParamXML * p);

        /*! \brief Destructor
         */
        ~MainWindow();

        /*! \brief Set ParamXML class
         * \param p : ParamXML object pointer
         */
        void setParam(ParamXML *p);

    public slots :
        /*! \brief Open a dialog window to select a file
         * 
         * Open a dialogue window to select an XML file and load it.
         */
        void actionOpenFile();

        /*! \brief Save parameters in file
         */
        void actionSaveFile();

        /*! \brief Open a dialog window to select a file to save parameters
         */
        void actionSaveFileAs();

        /*! \brief Close main window
         */
        void actionQuit();

    private :
        ParamXML * param; ///< Param object
        QTabWidget * tabs; ///< Mainwondow tabs
        InputParamWidget * iPW; ///< InputParam tab
        ParamParamWidget * pPW; ///< ParamParam tab
        OutputParamWidget * oPW; ///< OutputParam tab

        /*! \brief set the main widget
         * \param p : ParamXML object pointer
         *
         * Set and load main widget.
         */
        QWidget * setMainWidget(ParamXML * p);

        /*! \brief set menu and tool bar
         */
        void createMenuAndToolBar();

        /*! \brief Display value from ParamXML object
         * \param p : ParamXML objet pointer
         */
        void initValue(ParamXML *);


};

#endif
