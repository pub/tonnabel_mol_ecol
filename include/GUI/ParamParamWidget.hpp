/**
 * \file ParamParamWidget.hpp
 * \brief Parameters arguments editor
 * \author Jean-Francois Rey
 * \version 1.0
 * \date 04 Dec 2013
 */

#ifndef __PARAM_PARAM_WIDGET__
#define __PARAM_PARAM_WIDGET__

#include <string>
#include <iostream>
#include <QApplication>
#include <QWidget>
#include <QBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QPushButton>
#include <QObject>
#include <QComboBox>
#include <QString>
#include <QGridLayout>
//#include <QLine>
#include "ParamXML.hpp"
#include "Tools.hpp"

using namespace std;

/*! \class ParamParamWidget
 * \brief Class editor for parameters arguments
 *
 * The class implement an editor for parameters arguments values.
 */
class ParamParamWidget : public QWidget
{

    Q_OBJECT
    public : 
        /*! \brief Constructor
         *
         * ParamParamWidget constructor.
         */
        ParamParamWidget();

        /*! \brief Constructor
         * \param p : ParamXML object
         *
         * ParamParamWidget constructor.
         */
        ParamParamWidget(ParamXML * p);

        /*! \brief Destructor
         *
         * ParamParamWidget Destructor.
         */
        ~ParamParamWidget();

        /*! \brief Set "controler-modele" object
         * \param p : a ParamXML Object
         */
        void setParam(ParamXML *p);

        /*! \brief Initialize Widget
         *
         * Initialize widget with ParamXML value.
         */
        void initValue();

    public slots :

        /*! \brief Change Mode Dispersion value
         * \param s : value
         */
        void ChangeModeDist(const QString& s);
        
        /*! \brief Change Seed value
         * \param v : value
         */
        void ChangeSeed(int v);  

        /*! \brief Change Gama Initial value
         * \param v : value
         */
        void ChangeGamaInit(double);  

        /*! \brief Change Gama Minimum value
         * \param v : value
         */
        void ChangeGamaMin(double v);  

        /*! \brief Change Gama Maximun value
         * \param v : value
         */
        void ChangeGamaMax(double v);  

        /*! \brief Change Delta Initial value
         * \param v : value
         */
        void ChangeDeltaInit(double v);  
        
        /*! \brief Change Delta Minimum value
         * \param v : value
         */
        void ChangeDeltaMin(double v);  

        /*! \brief Change Delta Maximum value
         * \param v : value
         */
        void ChangeDeltaMax(double v);  

        /*! \brief Change Shape b Initial value
         * \param v : value
         */
        void ChangeShapeBInit(double v);  

        /*! \brief Change Shape b Minimum value
         * \param v : value
         */
        void ChangeShapeBMin(double v);  

        /*! \brief Change Shape b Maximum value
         * \param v : value
         */
        void ChangeShapeBMax(double v);  

        /*! \brief Change Migration Rate Initial value
         * \param v : value
         */
        void ChangeMigRateMInit(double v);  

        /*! \brief Change Migration Rate Minimum value
         * \param v : value
         */
        void ChangeMigRateMMin(double v);  

        /*! \brief Change Migration Rate Maximum value
         * \param v : value
         */
        void ChangeMigRateMMax(double v);  

        /*! \brief Change Selfting Rate s Initial value
         * \param v : value
         */
        void ChangeSelfRateSInit(double v);

        /*! \brief Change Selting Rate s Minimum value
         * \param v : value
         */
        void ChangeSelfRateSMin(double v);  

        /*! \brief Change Selfting Rate s Maximum value
         * \param v : value
         */
        void ChangeSelfRateSMax(double v);  

        /*! \brief Change BurnIn value
         * \param v : value
         */
        void ChangeBurnIn(int v);  

        /*! \brief Change Number of Iteration
         * \param v : value
         */
        void ChangeIte(int v);  

        /*! \brief Change Thin value
         * \param v : value
         */
        void ChangeThin(int v);

    private : 

        ParamXML * param;  ///< A ParamXML object

        QGridLayout * vLayout;  ///< Widget Main layout
        QComboBox * modeDist;   ///< Combo Box on Mode of Dispersion
        QSpinBox * seedSpinBox; ///< Seed value display
        QDoubleSpinBox * gamaInitSB, * gamaMinSB, * gamaMaxSB; ///< Gama values
        QDoubleSpinBox * deltaInitSB, * deltaMinSB, * deltaMaxSB;   ///< Delta vlaues
        QDoubleSpinBox * shapeBInitSB, * shapeBMinSB, * shapeBMaxSB;    ///< Shape b values
        QDoubleSpinBox * migRateMInitSB, * migRateMMinSB, * migRateMMaxSB;  ///< Migration Rate values
        QDoubleSpinBox * selfRateSInitSB, * selfRateSMinSB, * selfRateSMaxSB; ///< Self rate s 
        QSpinBox * burnInSB;    ///< BurnIn value
        QSpinBox * iteSB;   ///< number of iteration
        QSpinBox * thinSB;  ///< Thin/step value

};



#endif
