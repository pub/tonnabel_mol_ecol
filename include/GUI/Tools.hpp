/*! \file Tools.hpp
 * \brief Tools
 * \author Jean-Francois Rey
 * \date 16 Dec 2013
 */

#include <iostream>
#include <string>

/*! \class Tools
 * \brief A set of tools
 */
class Tools {

    public :
        /*! \brief Convert a point to a comma
         * \param str : a string
         * \return a string with last '.' converted to ","
         */
        static std::string convertPointToComma(std::string str);

        /*! \brief Convert a comma to a point
         * \param str : a string
         * \return a string with last ',' converted to '.'.
         */
        static std::string convertCommaToPoint(std::string str);

};
