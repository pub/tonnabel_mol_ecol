#ifndef __OUTPUT_PARAM_WIDGET__
#define __OUTPUT_PARAM_WIDGET__

/*!
 * \file OutputParamWidget.hpp
 * \brief QWidget containing output arguments
 * \author Jean-Francois REY
 * \version 1.0
 * \date 04 Dec 2013
 *
 * The OutputParamWidget class is a widget contaning argument editor for the output
 */

#include <string>
#include <iostream>
#include <QApplication>
#include <QWidget>
#include <QBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QSpinBox>
#include <QPushButton>
#include <QFileDialog>
#include <QObject>
#include <QComboBox>
#include <QFormLayout>
//#include <QLine>
#include "ParamXML.hpp"

using namespace std;

/*! \class OutputParamWidget
 * \brief class editor for output arguments
 *
 * The class implement an editor to output argument value.
 */
class OutputParamWidget : public QWidget
{

    Q_OBJECT
    public : 
        /*!
         * \brief Constructor
         *
         * OutputParamWidget constructor
         */
        OutputParamWidget();

        /*!
         * \brief Constructor
         * \param p : a ParamXML object
         *
         * OutputParamWidget constructor.
         * Set with a ParamXML p object.
         */
        OutputParamWidget(ParamXML * p);

        /*!
         * \brief Destructor
         *
         * OutputParamWidget destructor
         */
        ~OutputParamWidget();

        /*!
         * \brief Set "controler-modele"
         * \param p : a ParamXML object
         */
        void setParam(ParamXML * p);

        /*!
         * \brief Initialize Widget
         *
         * Initialize widget with ParamXML value.
         */
        void initValue();

    public slots :

        /*!
         * \brief Open a dialog window slot
         *
         * Open a dialog window to choose an output file for Gama
         */
        void ChooseGamaFile();

        /*!
         * \brief Open a dialog window slot
         * 
         * Open a dialog window to choose an output file for IndFecFile
         */
        void ChooseIndFecFile();

        /*!
         * \brief Open a dialog window slot
         *
         * Open a dialog window to choose an output file for Dispersion param.
         */
        void ChooseDispParamFile();

    private : 

        ParamXML * param; ///< A ParamXML pointer object.

        QFormLayout * vLayout; ///< widget main layout
        QLineEdit * oLEdit1; ///< LineEdit 1 parameter
        QLineEdit * oLEdit2; ///< LineEdit 2 parameter
        QLineEdit * oLEdit3; ///< LineEdit 3 parameter

};



#endif
