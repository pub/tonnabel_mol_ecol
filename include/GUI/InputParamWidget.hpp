/*!
 * \file InputParamWidget.hpp
 * \brief QWidget containing Input parameters arguements
 * \author Jean-Francois REY
 * \version 1.0
 * \date 16 Dec 2013
 *
 * The InputParamWidget class is a widget containing argument editor for the input parameters.
 */
#ifndef __INPUT_PARAM_WIDGET__
#define __INPUT_PARAM_WIDGET__

#include <string>
#include <iostream>
#include <QApplication>
#include <QWidget>
#include <QBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QSpinBox>
#include <QPushButton>
#include <QFileDialog>
#include <QObject>
#include <QComboBox>
#include <QGroupBox>
#include <QFormLayout>
//#include <QLine>
#include "ParamXML.hpp"
#include <string>
using namespace std;

/*!
 * \class InputParamWidget
 * \brief Class editor for Input parameters argument value.
 *
 * The class implement an editor for input paramters values.
 */
class InputParamWidget : public QWidget
{

    Q_OBJECT
    public : 
        /*! \brief Constructor
         *
         * Constructor
         */
        InputParamWidget();
        
        /*! \brief Constructor
         * \param p : a ParamXML object
         *
         * Constructor
         */
        InputParamWidget(ParamXML * p);

        /*! \brief Destructor
         *
         * Destructor
         */
        ~InputParamWidget();

        /*! \brief Set "controler-modele"
         * \param p : a ParamXML object
         */
        void setParam(ParamXML *p);

        /*! \brief Initialize Widget
         *
         * Initialize widget with ParamXML value.
         */
        void initValue();

    public slots :

        /*! \brief Dialog to choose Parent file
         */

        void ChooseFileParent();
        
        /*! \brief Dialog to choose Offspring file
         */
        void ChooseFileOffspring();

        /*! \brief Dialog to choose Allelic Frequencies file
         */
        void ChooseFileAF();

        /*! \brief Change Number of Locus for parent file
         * \param v : value
         */
        void ChangeLocusNumberParent(int v);

        /*! \brief Change Class Covariates value
         * \param v : value
         */
        void ChangeClassCovariatesParent(int v);

        /*! \brief Change Quantities Covariates value
         * \param v : value
         */
        void ChangeQtCovariatesParent(int v);

        /*! \brief Change Weight Var value
         * \param v : value
         * */
        void ChangeWeightVarParent(int v);

        /*! \brief Change number of locus for Offspring file
         * \param v : value
         */
        void ChangeLocusNumberOffspring(int v);

        /*! \brief Change Allelic Frequencies mode
         * \param s : value
         */
        void ChangeModeAF(const QString& s);

    private : 

        ParamXML * param;  ///< A ParamXML object

        QFormLayout * vLayout;  ///< Main widget Layout
        QLineEdit * pLEdit1;    ///< Parents file name
        QSpinBox * nlSpinBox;   ///< Number of Locus editor
        QSpinBox * ccSpinBox;   ///< Class covariates editor
        QSpinBox * qcSpinBox;   ///< Quantitative covariates editor
        QSpinBox * wvSpinBox;   ///< Weighting variables editor
        QLineEdit * oLEdit2;    ///< Offspring file name
        QSpinBox * nlSpinBox2;   ///< Number of Locus editor
        QLineEdit * afLEdit3;   ///< Allelic Frequencies file name
        QComboBox * modeAF;   ///< Allilec Frequencies mode


};



#endif
