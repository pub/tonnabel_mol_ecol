#ifndef MEMM_UTIL_H
#define MEMM_UTIL_H
#include <vector>

typedef struct PerePot{
  int pere_pot;
  double pere_prob;
} PerePot;

double gammln(double xx);

/*long double loglik1 (int nm, int ns, int npar,
                     const std::vector<double> & poids,
                     const std::vector<std::vector<double> > & distmp,
                     const std::vector<int> & meres, const std::vector<int> & merdesc, const std::vector <double > & probmig,
                     const std::vector <double > & probself, const std::vector <int > & nbperes, const std::vector<std::vector<PerePot> > & probperes,
                     const std::vector<double> & fec, double delta, double b, double mig, double self, long double & a, long double & ab);
*/

#endif
