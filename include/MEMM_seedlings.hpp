#ifndef __MEMM_SEEDLINGS_HPP__
#define __MEMM_SEEDLINGS_HPP__

#include "MEMM.hpp"

struct CouplePot { int mere_pot; int pere_pot; double couple_prob;};

/*! \class MEMM_seedlings
 * \brief MEMM_seedlings class
 *
 * Mother and Father are unknow.
 *
 */
class MEMM_seedlings : public MEMM {

  public :

    /*! \brief Constructor
     */
    MEMM_seedlings();

    /*! \brief destructor
     */
    virtual ~MEMM_seedlings();

    virtual void mcmc(int nbIteration, int thinStep);
  
  protected :
    vector<vector<double>> DistSM;  //!< Distances between Seed Mother
    vector<int> NbMeres;  //!< number of mothers for a seed i
    vector<int> NbPeres;  //!< number of fathers for a seed i
    vector<int> NbCouples;  //!< number of couple (mother-father) for a seed i
    vector<double> ProbMig;   //!< proba of outside site fertilization
    vector<vector< PerePot >> ProbMeres;  //!< potential mothers for a seed i
    vector<vector< PerePot >> ProbPeres;  //!< potential fathers for a seed i
    vector<vector< CouplePot >> ProbCouples;  //!< potential couples for a seed i

    virtual void loadSeeds(Parameters * p);
    virtual void calculDistances(Parameters *p);
    virtual void calculTransitionMatrix(Parameters *p);
    virtual void loadPrior(Parameters * p);
    virtual void loadParameters(Parameters *p);

    /*! \brief Calcul model log likelihood
     */
    long double logLik();
    long double logLikVerbose();
  
    

    double Scales;  //!< scales parameter initial, dimension of distance (as);
    double mScales; //!< min scales value
    double MScales; //!< max scales value
    double Shapes;  //!< shapes parameter initial value, dispersal kernel (bs)
    double mShapes; //!< min shapes value
    double MShapes; //!< max shapes value
    
    double Migs;  //!< migs initial value;
    double mMigs; //!< min migs value
    double MMigs; //!< max migs value
    double GamAs; //!< GamAs initial value
    double mGamAs;//!< min GamAs value
    double MGamAs;//!< max GamAs value
    double as; //!< mean dispersal distance (deltas)
    double abs; //!< exponential power dispersal kernel (1/as)^bs
    double ap; //!<  mean dispersal distance (delta)
    double abp; //!< exponential power dispersal kernel (1/a)^b

    double Imp; //!< Imp initial value; (pollen sortant de la parcelle puis graine entrante)
    double mImp;
    double MImp;
    
    //Début modif
    double Pi0s, mPi0s, MPi0s; // initial, min and max values for the zero-inflated parameter (seed)
    // Fin modif

    vector<double> Vec_Fecs;  //!< mother fecundity
    vector<double> Vec_Fecp;  //!< male fecunditiy
    
    MEMM_loi * pLois; //!< distribution for individual fecundities

 private :
    
};

#endif

