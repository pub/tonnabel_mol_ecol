#ifndef PARENT_H
#define PARENT_H

/*!
 * \file parent.hpp
 * \brief Parent class
 * \author Etienne Klein
 * \author Jean-Francois Rey
 * \version 1.5
 * \date 21 April 2015
 */

#include <individu.h>
#include <vector>
#include <iostream>
#include <string>
#include <fstream>
#include <math.h>

/*!
 * \class parent
 * \brief parent class
 *
 * This class define a parent. it inherites from individu with coordinates and corvar.
 */
class parent : public individu
{

    public:

        /*! \brief Constructor
         * \param name : individu name
         * \param x : abscissa coordinate
         * \param y : ordinate coordinate
         * \param cov1 : list of covar quali
         * \param cov2 : list of covar quanti
         */
        parent(const std::string & name, double x, double y, std::vector<int> & cov1, std::vector<double> & cov2);

        /*! \brief Constructor
         * \param file_in : in stream file
         * \param nquali : number of covar quali to load
         * \param nquanti : number of covar quanti to load
         * \param nloc : number of locus to load
         *
         * Will load parent data from file_in.
         */
        parent(std::ifstream & file_in, int nquali, int nquanti, int nloc);

        /*! \brief Constructor
         * \param name : individu name
         * \param file_in : in stream file
         * \param nquali : number of covar quali
         * \param nquanti : number of covar quanti
         * \param nloc : number of locus
         *
         * Will load parent data from file_in.
         */
        parent(const std::string & name, std::ifstream & file_in, int nquali, int nquanti, int nloc);

        /*! \brief Destructor
         */
        virtual ~parent();

        /*! \brief get distance between an another parent
         * \param pere : a parent
         * \return distances between the two parents.
         */
        double dist(const parent & pere) const;

        /*! \brief print parent information
         */
        virtual void afficher() const;

        /*! \brief get a covar quali value
         * \param i : covar index
         * \return a covar value
         */
        int getCoVarQuali(unsigned int i){ return( (i < _covarquali.size()) ? _covarquali[i] : 0);}
      
        /*! \brief get a covar quanti value
         * \param i : covar index
         * \return a covar value
         */
        double getCoVarQuanti(unsigned int i){ return( (i < _covarquanti.size()) ? _covarquanti[i] : 0);}

    protected:
        double _x, _y;  //!< coordinates
        std::vector<int> _covarquali; //!< covar quali vector
        std::vector<double> _covarquanti; //!< covar quanti vector

};

#endif // PARENT_H
