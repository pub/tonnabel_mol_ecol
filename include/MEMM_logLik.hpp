#ifndef __MEMM_LOGLIK_HPP__
#define __MEMM_LOGLIK_HPP__

/*!
 * \file MEMM_logLik.hpp
 * \brief MEMM_logLik class
 * \author Jean-Francois REY
 * \version 1.0
 * \date 23 Sept 2014
 *
 * The MEMM_loglik class enables to estimate the pollen dispersal kernel and the variance in male fecundity from spatial and genetic data (microsatellites) concerning adult plants and sampled seeds (see Klein et al. 2008 Molecular Ecology).
 */

#include "MEMM.hpp"

using namespace std;

/*! \class MEMM_logLik
 * \brief MEMM_logLik estimate pollen dispersal kernel and the variance in male fecundity
 *
 * This class enables to estimate the pollen dispersal kernel and the variance in male fecundity from spatial and genetic data (microsatellites) concerning adult plants and sampled seeds (see Klein et al. 2008 Molecular Ecology).
 */
class MEMM_logLik : public MEMM {

  public :

    /*! \brief Constructor
     *
     * MEMM_logLik constructor
     */
    MEMM_logLik();

    /*! \brief Destructor
     *
     * MEMM_logLik class destructor
     */
    ~MEMM_logLik();

    virtual void mcmc(int nbIteration, int thinStep);

  protected :
    vector<int> MerDesc;  //!< list of mother ID of descendants
    vector<int> Meres;    //!< mother ID to father ID
    map<string,int> IDmere; //!< mother name and id
    int Nm;   //!< number of mothers
    vector<double> ProbSelf;  //!< proba of self fertilization
    vector<double> ProbMig;   //!< proba of outside site fertilization
    vector<vector<PerePot>> ProbPeres;  //!< proba for each seeds to be fertilizated by site father
    vector<int> NbPeres;    //!< number of posible father for a seed

    // Prior
    double a;   //!< mean dispersal distance (delta)
    double ab;  //!< exponential power dispersal kernel (1/a)^b
    vector<double> Vec_Fec; //!< male fecunditiy

    virtual void loadSeeds(Parameters * p);
    virtual void calculDistances(Parameters * p);
    virtual void calculTransitionMatrix(Parameters *p);
    virtual void loadPrior(Parameters *p);

    /*! \brief Calcul the likelihood of the model
     *
     * Will calcul the log likelihood of the model
     */
    long double compute();
  private :


};



#endif
