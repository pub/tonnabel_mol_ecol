#ifndef __MEMM_HPP__
#define __MEMM_HPP__

/*!
 * \file MEMM.hpp
 * \brief MEMM abstract class
 * \author Jean-Francois REY
 * \version 1.0
 * \date 23 Sept 2014
 *
 * The MEMM abstract class define common variables, tools and models.
 */


#include <boost/random.hpp>
#include <boost/random/variate_generator.hpp>
#include <boost/random/lagged_fibonacci.hpp>
#include <boost/random/normal_distribution.hpp>
#include <boost/random/uniform_real_distribution.hpp>

#include "Parameters.hpp"
#include "ParamTXT.hpp"
#include "ParamXML.hpp"
#include "parent.h"
#include "graine.h"
#include "MEMM_util.h"
#include "MEMM_loi.h"
#include "MEMM_logNormal.h"
#include "MEMM_gamma.h"
#include "MEMM_zigamma.h"
#include <time.h>

using namespace std;

/*! \class MEMM
 * \brief MEMM main Abstract class
 *
 * This class define the main Mixed Effect Mating Model variables and tools.
 */
class MEMM {

  public :
    
    /*!i \brief Constrcutor
     *
     * MEMM class constructor
     */
    MEMM();

    /*! \brief Destructor
     *
     * MEMM class destructor
     */
    virtual ~MEMM();

    /*! \brief initialize variables from parameters
     * \param p : a Parameters pointer
     *
     * Load parameters and initialize variables and model.
     */
    virtual void init(Parameters * p);

    /*! \brief run mcmc
     *  Run mcmc algorithm with nbIteration and thin 
     */
    void run();

    /*! \brief run burn in
     * Run mcmc with burn in iteration
     */
    void burnin();

    /*! \brief mcmc algorithm
     * \param nbIteration : number of iteration
     * \param thinStep : number of step for output (0 print nothings)
     *
     * Run mcmc algorithm. 
     */
    virtual void mcmc(int nbIteration, int thinStep) = 0;

  protected : 
    MEMM_loi * pLoi;  //!< distribution for individual fecundities
    int NPar; //!< number of parents
    int Ns;   //!< number of seeds

    int NFath; //!< number of males
    int NMoth; //!< number of females
    
    
    vector<vector<double>> DistMP;  //!< Distances between individu
    vector<double> Poids;   //!< pollen donor weight 


    //Prior
    double Scale; //!< scale parameter initial, dimension of distance (a)
    double mScale; //!< min value of scale parameter, dimension of distance (a)
    double MScale; //!< max value of scale parameter, dimension of distance (a)
    double Shape; //!< shape parameter initial for dispersal kernel (b)
    double mShape; //!< min value of shape parameter for dispersal kernel (b)
    double MShape; //!< max value of shape parameter for dispersal kernel (b)
    double Mig;   //!< initial value of probabilities to mother to be fertilized by pollen grain from uncensored father form outside the area
    double mMig;  //!< min value of probabilities to mother to be fertilized by pollen grain from uncensored father form outside the area
    double MMig;  //!< max value of probabilities to mother to be fertilized by pollen grain from uncensored father form outside the area
    double Self;  //!< initial probabilites to mother to be self fertilized
    double mSelf; //!< min value probabilites to mother to be self fertilized
    double MSelf; //!< max value probabilites to mother to be self fertilized
    double GamA;  //!< initial LogNormal fecundities exp(variance) = dobs/de
    double mGamA; //!< min value LogNormal fecundities exp(variance) = dobs/de
    double MGamA; //!< max value LogNormal fecundities exp(variance) = dobs/de

    double Pi0, mPi0, MPi0; //!< initial, min and max values for the zero-inflated parameter

    
    int Nburn;  //!< Burn in number
    int Nstep;  //!< number of iteration
    int Nthin;  //!< thinning step

    ofstream * ParamFec;  //!< output stream step | logLik of all fecundities | GamaA
    ofstream * IndivFec;  //!< output stream individual fecundities step | Vec_Fec[i]  
    ofstream * ParamDisp; //!< output stream dispersal parameters step | LogLik | scale | shape | Mig | Self | delta | ab

    boost::normal_distribution<> dist_norm;
    boost::uniform_real<> unif;
    boost::lagged_fibonacci19937 generator;
    boost::variate_generator<boost::lagged_fibonacci19937&, boost::normal_distribution<> > gauss;
    boost::variate_generator<boost::lagged_fibonacci19937&, boost::uniform_real<> > accept;

    // Variables use at initialisations only
    vector<parent*> allParents; //!< All parents from file
    map<string,int> fatherID; //!< father name - ID
    vector<int> MothList; // Liste des positions des femelles dans la liste des parents
    vector<int> FathList; // Liste des positions des males dans la liste des parents
    
    
    int Nl; //!< number of locus
    int Nql;  //!< number of class covariates
    int Nqt;  //!< number of quantitative covariates
    int Nw; //!< variables weight
    vector<individu*> allSeeds; //!< All seeds from file
    vector< std::vector<double> > AllFreq;  //!< [locus][alleles] frequencies  
    vector< std::map<int,int> > SizeAll;  //!< [locus] <alleles-ID>
    bool useMatError;
    int NMistypeMax;
    vector< vector < vector <double> > > MatError;  //!< [locus][alleles][alleles] <proba de lien lu/vrai>
    vector<int> Na; //!< <locus-ID> number of allele


    /*! \brief load parent file
     * \param p : a Parameters pointer
     * 
     * Will load parents information into AllParents.
     */
    virtual void loadParentFile(Parameters * p);
    
    /*! \brief load seed file
     * \param p : a Parameters pointer
     * 
     * Will load seeds information into AllSeeds.
     */
    virtual void loadSeeds(Parameters * p);

    /*! \brief Calcul distances between parents
     * \param p : a Parameters pointer
     * 
     * Calcul distances between parents (DispMP).
     */
    virtual void calculDistances(Parameters *p);

    /*! \brief load Allelic Frequencies
     * \param p : a Parameters pointer
     * 
     * Will load Allelic frequencies from file or calcul it.
     */
    virtual void loadAllelicFreq(Parameters * p);

    /*! \brief load locus error matrix
     * \param p : a parameterq pointer
     *
     * Will create a matrix of error for all locus (MatError)
     */
    virtual void loadMatrixError(Parameters * p, vector< vector<int> > & AllSize);

    /*! \brief Calcul Trnasition Matrix
     * \param p : a Parameters pointer
     * 
     * Calcul transition matrix.
     */
    virtual void calculTransitionMatrix(Parameters *p);

    /*! \brief load prior
     * \param p : a Parameters pointer
     * 
     * Will load and initialize prior variables.
     */
    virtual void loadPrior(Parameters *p);

    /*! \brief load paramerters
     * \param p : a Parameters pointer
     * 
     * Will load parameters
     */
    virtual void loadParameters(Parameters *p);

    /*! \brief Calcul Pollen donor weight
     * 
     * Calcul pollen donor weight.
     */
    virtual void calculWeightPollenDonor();

  private :

};

#endif
