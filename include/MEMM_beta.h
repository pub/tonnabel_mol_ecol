#ifndef MEMM_BETA_H
#define MEMM_BETA_H
#include <fstream>
#include <vector>
#include <boost/random/variate_generator.hpp>
#include <boost/random/lagged_fibonacci.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/math/distributions/beta.hpp>
#include <boost/math/special_functions/beta.hpp>
#include "MEMM_loi.h"
/** 
 * \brief Cette classe implemente la loi gamma.
 */
class MEMM_beta : public MEMM_loi
{
 private:
    double _palpha, _pbeta;
    boost::math::beta_distribution<> _dbeta;
    boost::variate_generator<boost::lagged_fibonacci19937&, boost::uniform_real<> >& _accept;
 public:
  /** Constructeur
      \palpha permentant de construire la loi beta(alpha,beta).
   \pbeta permentant de construire la loi beta(alpha,beta).
   */
  MEMM_beta(double& palpha, double& pbeta,
	     boost::variate_generator<boost::lagged_fibonacci19937&, boost::uniform_real<> >& accept);
  /** faire un tirage
      \palpha [out] valeur du tirage.
      \pbeta [out] valeur du tirage.
  */
  virtual void tirage(double& v);
  /** Calcul la vraisemblance de l'echantillon ech avec les parametres de loi palpha et pbeta
   * \param [in] npar, taille de l'echantillon
   * \param [in] prop, vecteur de proportions
   * \palpha [in] parametre de loi beta
   * \pbeta [in] parametre de loi beta
   * \return la vraisemblance
   */
  virtual double logLik(int npar,const std::vector<double> & prop, double& palpha, double & pbeta);
virtual double logLik(int npar,const std::vector<double> & prop, double& palpha);
  /** Modifier le parametre d'indice indiceParam.
      \param [in] indice du parametre (MEMM_LOI_GAMA pour gamma(1/v,v)).
      \param [in] valeur a prendre en compte.
   */
  virtual void setDParam(unsigned int indiceParam, double & value);
  virtual ~MEMM_beta();
};

#endif // MEMM_BETA_H
