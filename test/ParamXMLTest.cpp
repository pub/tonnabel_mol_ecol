#include <gtest/gtest.h>
#include <iostream>
#include <fstream>
#include "ParamXMLTest.hpp"
#include "../include/ParamXML.hpp"

ParamXMLTest::ParamXMLTest(){
        pxml = new ParamXML((char*)"./ParametersTest.xml");
    };

ParamXMLTest::~ParamXMLTest(){
  delete pxml;
    };


TEST_F(ParamXMLTest, ParametersValue)
{
    EXPECT_STREQ("AlisierParTest.txt",pxml->getValue(MEMM_PARENTS_FILE_NAME));
    EXPECT_STREQ("6",pxml->getValue(MEMM_PARENTS_FILE_NUMBER_OF_LOCUS));
    EXPECT_STREQ("6",pxml->getValue(MEMM_PARENTS_FILE_CLASS_COV));
    EXPECT_STREQ("2",pxml->getValue(MEMM_PARENTS_FILE_QT_COV));
    EXPECT_STREQ("1",pxml->getValue(MEMM_PARENTS_FILE_WEIGHT_VAR));
    EXPECT_STREQ("AlisierDescTest.txt",pxml->getValue(MEMM_OFFSPRING_FILE_NAME));
    EXPECT_STREQ("6",pxml->getValue(MEMM_OFFSPRING_FILE_NUMBERS_OF_LOCUS));
    EXPECT_STREQ("file",pxml->getValue(MEMM_AF_FILE_MODE));
    EXPECT_STREQ("freqallTest.txt",pxml->getValue(MEMM_AF_FILE_NAME));
    EXPECT_STREQ("LocusError.txt",pxml->getValue(MEMM_LOCUS_ERROR_FILE));
    EXPECT_STREQ("LN",pxml->getValue(MEMM_IND_FEC_DIST));
    EXPECT_STREQ("12345",pxml->getValue(MEMM_SEED));
    EXPECT_STREQ("2.0",pxml->getValue(MEMM_GAMA_INIT));
    EXPECT_STREQ("1.0",pxml->getValue(MEMM_GAMA_MIN));
    EXPECT_STREQ("1000.0",pxml->getValue(MEMM_GAMA_MAX));
    EXPECT_STREQ("100.0",pxml->getValue(MEMM_DELTA_INIT));
    EXPECT_STREQ("0.0",pxml->getValue(MEMM_DELTA_MIN));
    EXPECT_STREQ("10000.0",pxml->getValue(MEMM_DELTA_MAX));
    EXPECT_STREQ("1.0",pxml->getValue(MEMM_SHAPE_B_INIT));
    EXPECT_STREQ("0.1",pxml->getValue(MEMM_SHAPE_B_MIN));
    EXPECT_STREQ("10.0",pxml->getValue(MEMM_SHAPE_B_MAX));
    EXPECT_STREQ("0.5",pxml->getValue(MEMM_MIG_RATE_M_INIT));
    EXPECT_STREQ("0.1",pxml->getValue(MEMM_MIG_RATE_M_MIN));
    EXPECT_STREQ("1.0",pxml->getValue(MEMM_MIG_RATE_M_MAX));
    EXPECT_STREQ("0.05",pxml->getValue(MEMM_SELF_RATE_S_INIT));
    EXPECT_STREQ("0.0",pxml->getValue(MEMM_SELF_RATE_S_MIN));
    EXPECT_STREQ("0.1",pxml->getValue(MEMM_SELF_RATE_S_MAX));
    EXPECT_STREQ("5000",pxml->getValue(MEMM_BURNIN));
    EXPECT_STREQ("50000",pxml->getValue(MEMM_ITE));
    EXPECT_STREQ("20",pxml->getValue(MEMM_THIN));
    EXPECT_STREQ("ParamFecTest.txt",pxml->getValue(MEMM_GAMA_FILE_NAME));
    EXPECT_STREQ("IndivFecTest.txt",pxml->getValue(MEMM_IND_FEC_FILE_NAME));
    EXPECT_STREQ("ParamDispTest.txt",pxml->getValue(MEMM_DISP_FILE_NAME));

}

