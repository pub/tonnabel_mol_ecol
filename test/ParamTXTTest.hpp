#ifndef __PARAM_TXT_TEST__
#define __PARAM_TXT_TEST__

#include <gtest/gtest.h>
#include <iostream>
#include <fstream>
#include "../include/ParamTXT.hpp"

class ParamTXTTest : public ::testing::Test {

    protected:
    ParamTXTTest();
    
    virtual ~ParamTXTTest();

    virtual void SetUp() {
        // Code here will be called immediately after the constructor (right
            //     // before each test).
     }
    
    virtual void TearDown() {
    // Code here will be called immediately after each test (right
    // before the destructor).
    }

    ParamTXT * ptxt;

};

#endif

