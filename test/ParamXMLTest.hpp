#ifndef __PARAM_XML_TEST__
#define __PARAM_XML_TEST__

#include <gtest/gtest.h>
#include <iostream>
#include <fstream>
#include "../include/ParamXML.hpp"

class ParamXMLTest : public ::testing::Test {

    protected:
    ParamXMLTest();
    
    virtual ~ParamXMLTest();

    virtual void SetUp() {
        // Code here will be called immediately after the constructor (right
            //     // before each test).
     }
    
    virtual void TearDown() {
    // Code here will be called immediately after each test (right
    // before the destructor).
    }

    ParamXML * pxml;

};

#endif

